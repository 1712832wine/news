<?php

use App\Http\Livewire\AlbumAsset\AssetComponent;
use Illuminate\Support\Facades\Route;
use App\Http\Livewire\Menu\MenuComponent;
use App\Http\Livewire\Pages\PagesComponent;
use App\Http\Livewire\Tags\TagsComponent;
use App\Http\Livewire\Articles\ArticlesComponent;
use App\Http\Livewire\Categories\CategoriesComponent;
use App\Http\Livewire\Users\UsersComponent;
use App\Http\Livewire\Roles\RolesComponent;
use App\Http\Livewire\Permissions\PermissionsComponent;
use App\Http\Livewire\Albums\AlbumsComponent;
use App\Http\Livewire\Contacts\ContactsComponent;
use App\Http\Livewire\Recruitments\RecruitmentsComponent;
use App\Http\Livewire\FileManager\FileManager;
use App\Http\Livewire\Products\ProductsComponent;
// ------------------------------------------------------------------------
use App\Http\Livewire\Frontend\AddContact;
use App\Http\Livewire\Frontend\News;
use App\Http\Livewire\Frontend\Recruitment;
// ------------------------------------------------------------------------\
use App\Http\Controllers\Controller;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// ------------------------------------------------------- //
Route::middleware(['auth:sanctum', 'verified'])->prefix('admin')->group(function () {
    Route::get('/dashboard', function () { return view('dashboard'); })->name('dashboard');
    Route::get('/menu', MenuComponent::class)->name('menu');
    Route::get('/pages', PagesComponent::class)->name('pages');
    Route::get('/news/tags', TagsComponent::class)->name('tags');
    Route::get('/news/categories', CategoriesComponent::class)->name('categories');
    Route::get('/news/articles', ArticlesComponent::class)->name('articles');
    Route::get('/file_manager',FileManager::class)->name('file_manager');
    Route::get('/authentication/users', UsersComponent::class)->name('users');
    Route::get('/authentication/roles', RolesComponent::class)->name('roles');
    Route::get('/authentication/permissions', PermissionsComponent::class)->name('permissions');
    Route::get('/albums', AlbumsComponent::class)->name('albums');
    Route::get('/contacts', ContactsComponent::class)->name('contacts');
    Route::get('/recruitments', RecruitmentsComponent::class)->name('recruitments');
    Route::get('/products', ProductsComponent::class)->name('products');
    Route::get('/assets/{id}', AssetComponent::class)->name('assets');
});
Route::group([], function () {
    Route::get('/', [Controller::class,'home'])->name('home');
    Route::get('/introduce', [Controller::class, 'introduce'])->name('introduce');
    Route::get('/gioi-thieu', [Controller::class, 'introduce'])->name('introduce');
    Route::get('/news', News::class)->name('news');
    Route::get('/tin-tuc', News::class)->name('news');
    Route::get('/news/{slug}', [Controller::class, 'newsdetail'])->name('newsdetail');
    Route::get('/project', [Controller::class, 'project'])->name('project');
    Route::get('/du-an', [Controller::class, 'project'])->name('project');
    Route::get('/recruitment', Recruitment::class)->name('recruitment');
    Route::get('/tuyen-dung', Recruitment::class)->name('recruitment');
    Route::get('/contact',AddContact::class)->name('contact');
    Route::get('/lien-he',AddContact::class)->name('contact');

});
