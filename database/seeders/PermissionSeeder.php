<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Permission;
class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $list = ['manage permissions',	 
        'manage roles',	 
        'manage users',	 
        'manage menu',	 
        'manage pages',	 
        'manage news',
        'manage albums'];
        
        for($i=0;$i<7;$i++){
            $permission = new Permission();
            $permission->name = $list[$i];
            $permission->save();
        }
    }
}
