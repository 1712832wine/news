<div>

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="{{ url('frontend') }}/css/home-housecss.css">
        <link rel="stylesheet" type="text/css" href="{{ url('frontend') }}/css/contactcss.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
        <link
            href="https://fonts.googleapis.com/css2?family=Bai+Jamjuree:ital,wght@0,200;0,300;0,500;0,600;1,200;1,300;1,400;1,500;1,600;1,700&display=swap"
            rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css" />
        <link rel="icon" type="image/png" href="./image/logo.png" />
        <title>Home&House</title>
    </head>

    <body>
        @include('frontend.component.header')
        <!-- ============================================================= START INFO ======================================================================================== -->
        <div class="container-fluid">
            <div class="contact">
                <div class="container">
                    <div class="tittle">
                        <h4>Liên hệ</h4>
                        <p>Bạn đang phân vâng về việc trang trí hay thi công phòng ở. Hoặc thắc mắc về giá cả bản vẽ
                            thiết
                            kế thì hãy liên hệ ngay với Home&house. Chúng tôi sẽ hỗ trợ và giải đáp thắc mắc của bạn một
                            cách chi tiết nhất! </p>
                    </div>
                    <form class="info">
                        <div class="info-input" style="margin-right: 0.5vw;">
                            <div class="info-line">
                                <h6>Họ và tên</h6>
                                <input type="text" placeholder="Nhập họ tên" wire:model.defer="name">
                            </div>
                            <div class="info-line">
                                <h6>Email</h6>
                                <input type="text" placeholder="Nhập email" wire:model.defer="email">
                            </div>
                            <div class="info-line">
                                <h6>Địa chỉ</h6>
                                <input type="text" placeholder="Nhập địa chỉ" wire:model.defer="address">
                            </div>
                            <div class="info-line">
                                <h6>Nội dung cần hỗ trợ</h6>
                                <textarea rows="5" cols="40" placeholder="Nhập nội dung"
                                    wire:model.defer="content"></textarea>
                            </div>
                        </div>
                        <div class="info-image">
                            <img src="{{ url('frontend') }}/image/contact-img.png" alt="" id="setheight"
                                style="height:350.25px;">
                        </div>
                        <div class="break"></div>
                    </form>
                    <div class="info-submit" wire:click.prevent="saveAndBack()">
                        <input type="submit">
                    </div>
                </div>
            </div>
        </div>

        <div class=" container-fluid">
            <div class="contact-map">
                <iframe
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1959.6428317937136!2d106.62427340966927!3d10.789419256429126!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752c0484508c83%3A0xcbfcaf1b15762d6f!2zUGjhuqFtIFbEg24gWOG6o28sIFTDom4gUGjDuiwgVGjDoG5oIHBo4buRIEjhu5MgQ2jDrSBNaW5oLCBWaWV0bmFt!5e0!3m2!1sen!2s!4v1603820640086!5m2!1sen!2s"
                    width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false"
                    tabindex="0"></iframe>
            </div>
        </div>

        <!-- ============================================================= END INFO =========================================================================================== -->
        <!-- ============================================================= START FOOTER ======================================================================================= -->
        @include('frontend.component.footer')
    </body>
</div>
