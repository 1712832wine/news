<div>
    @include('frontend.component.header')
    <!-- ============================================================= START INFO ======================================================================================== -->

    <div class="container-fluid">
        <div class="news-header">
            <div class="news-search">
                <input type="text" placeholder="Bố trí nội thất …" wire:model="current" wire:keydown.enter="Do()">
                <button type=" submit" wire:click="Do()">
                    <img src=" {{ url('frontend') }}/image/news-search.png" alt="">
                </button>
            </div>
        </div>
    </div>

    @php
        if ($this->search != '') {
            $status = 'active';
            $searchs = 'disable';
        } else {
            $status = 'disable';
            $searchs = 'active';
        }
    @endphp
    <div class="container-fluid {{ $status }} ">
        <div class="container">
            <div class="news-info new">
                <div class="tittle">
                    <h4>Kết quả tìm kiếm liên quan tới "{{ $search }}"</h4>
                </div>
                @php
                    $count = count($search_articles);
                @endphp
                <div class="row blog">
                    @for ($i = 0; $i < $count; $i++)
                        <div class="col-md-4">
                            <div class="news-link" id="news-1-{{ $i }}">
                                <a href="{{ route('newsdetail', [$search_articles[$i]['slug']]) }}">
                                    <div class="image">
                                        <img src="{{ url('storage/Articles') }}/{{ explode(',', $search_articles[$i]['image'])[0] }}"
                                            alt="">
                                    </div>
                                </a>
                                <div class="text">
                                    <a href="{{ route('newsdetail', [$search_articles[$i]['slug']]) }}">
                                        <p class="bold">{{ $search_articles[$i]['title'] }}</p>
                                    </a>
                                    <p class="time">{{ $search_articles[$i]['created_at'] }}</p>
                                    <div class="content" style="max-height: 41vh;
                                                            overflow: hidden; font-size: 0.73vw;">
                                        {!! $search_articles[$i]['content'] !!}</div>
                                    <a href="{{ route('newsdetail', [$search_articles[$i]['slug']]) }}">xem
                                        thêm</a>
                                </div>
                            </div>
                        </div>
                    @endfor
                </div>
                {{ $search_articles->links() }}
            </div>
        </div>
    </div>
    <div class="container-fluid {{ $searchs }}">
        <div class="container">
            <div class="news-info new">
                <div class="tittle">
                    <h4>Các bài viết mới nhất</h4>
                    <p>Những bài viết được cập nhật mới nhất và nhận được tương tác cao của khách hàng</p>
                </div>
                @include('frontend.component.carousel',['articles'=>$interracts_articles,'id'=>2])
            </div>
        </div>
    </div>
    <div class="container-fluid {{ $searchs }}">
        <div class="news-info done">
            <div class="container">
                <div class="tittle">
                    <h4>Các bài viết đã qua</h4>
                    <p>Những bài viết Home&house gửi đến bạn đọc</p>
                </div>
                @include('frontend.component.carousel',['articles'=>$articles,'id'=>3])

            </div>
        </div>
    </div>
    @include('frontend.component.footer')
</div>
