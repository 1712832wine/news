<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{{ url('frontend') }}/css/home-housecss.css">
    <link rel="stylesheet" type="text/css" href="{{ url('frontend') }}/css/projectcss.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
    <link
        href="https://fonts.googleapis.com/css2?family=Bai+Jamjuree:ital,wght@0,200;0,300;0,500;0,600;1,200;1,300;1,400;1,500;1,600;1,700&display=swap"
        rel="stylesheet">
    <title>Home&House</title>
</head>

<body>
    @include('frontend.component.header')
    <!-- ============================================================= START INFO ======================================================================================== -->
    <div class="container-fluid">
        <div class="project-tittle">
            <h4>Các dự án đã thực hiện tại</h4>
            <h1>HOME&HOUSE</h1>
            <div class="sticky"></div>
            <p> <strong>Home&house </strong> tự hào thiết kế và thi công nội thất nhà ở cho các ngôi nhà thuộc nhiều
                tầng lớp ở Việt Nam, giúp không gian sống của khách hàng trở nên tuyệt vời hơn bằng việc kiến tạo
                môi
                trường sống, làm việc đầy cảm hứng và phản ánh đặc trưng của khách hàng.</p>

        </div>
    </div>

    <div class="container-fluid">
        @foreach ($albums as $album_index => $album)
            <div class="project-1">
                <div class="container">
                    <div class="tittle">
                        <h6>{{ $album['name'] }}</h6>
                        <p>{{ $album['short_desc'] }}</p>
                    </div>
                    <div class="img">
                        @php
                            switch ($album['template']) {
                                case 'template 1':
                                    $template = [3, 2, 2];
                                    break;
                                case 'template 2':
                                    $template = [3, 0, 2];
                                    break;
                                default:
                                    # code...
                                    break;
                            }
                            $album_size = count($lists[$album_index]);
                            $number_line = floor($album_size / array_sum($template)) * count($template);
                            $count = 0;
                            $residual = $album_size % array_sum($template);
                        @endphp
                        {{-- MOBILE --}}
                        @for ($i = 0; $i < $album_size; $i++)
                            <a href="{{ url('storage') }}/photos/{{ $lists[$album_index][$i]['url'] }}"> <img
                                    src="{{ url('storage') }}/photos/{{ $lists[$album_index][$i]['url'] }}" alt=""
                                    class="img-mobile"></a>
                        @endfor
                        {{-- BEFORE --}}
                        @for ($i = 0; $i < $number_line; $i++)
                            @for ($k = 0; $k < count($template); $k++)  @if ($i % 3===0)
                                <div class="line-{{ $k + 1 }}">
                                @for ($j = 0; $j < $template[$k]; $j++)
                                    <div class="project-hover">
                                    <a
                                    href="{{ url('storage') }}/photos/{{ $lists[$album_index][$count]['url'] }}">
                                    <img
                                    src="{{ url('storage') }}/photos/{{ $lists[$album_index][$count]['url'] }}"
                                    alt=""></a>
                                    </div>
                                    @php
                                        $count += 1;
                                    @endphp @endfor
                    </div>
        @endif
        @endfor
        @endfor
        {{-- AFTER --}}
        @php
            $line = 0;
            $template_line;
        @endphp
        @while ($residual !== 0)
            <div class="line-{{ $line + 1 }}">
                @if ($residual < $template[$line])
                    <!--LESS========================================================-->
                    @php
                        $temp = $residual;
                    @endphp
                    @for ($v = 0; $v < $temp; $v++)
                        <div class="project-hover">
                            <a href="{{ url('storage') }}/photos/{{ $lists[$album_index][$count]['url'] }}">
                                <img src="{{ url('storage') }}/photos/{{ $lists[$album_index][$count]['url'] }}"
                                    alt=""></a>
                        </div>
                        @php
                            $count += 1;
                            $residual -= 1;
                        @endphp
                    @endfor
                    <!--================================================================-->
                @else
                    @for ($v = 0; $v < $template[$line]; $v++)
                        <div class="project-hover">
                            <a href="{{ url('storage') }}/photos/{{ $lists[$album_index][$count]['url'] }}">
                                <img src="{{ url('storage') }}/photos/{{ $lists[$album_index][$count]['url'] }}"
                                    alt=""></a>
                        </div>
                        @php
                            $count += 1;
                            $residual -= 1;
                        @endphp
                    @endfor
                    @php
                        $line += 1;
                    @endphp
                @endif
            </div>
        @endwhile
    </div>
    </div>
    </div>
    @endforeach
    </div>
    <!-- ============================================================= END INFO =========================================================================================== -->

    <!-- ============================================================= START FOOTER ======================================================================================= -->
    @include('frontend.component.footer')

</body>

</html>
