@php
$menu = [
    [
        'title' => 'Trang chủ',
        'href' => route('home'),
        'name' => 'home',
    ],
    [
        'title' => 'Giới thiệu',
        'href' => route('introduce'),
        'name' => 'introduce',
    ],
    [
        'title' => 'Dự án',
        'href' => route('project'),
        'name' => 'project',
    ],
    [
        'title' => 'Tin tức',
        'href' => route('news'),
        'name' => 'news',
    ],
    [
        'title' => 'Tuyển dụng',
        'href' => route('recruitment'),
        'name' => 'recruitment',
    ],
    [
        'title' => 'Liên hệ',
        'href' => route('contact'),
        'name' => 'contact',
    ],
];
@endphp
<section id="sc-home">

    <nav class="navbar">
        <div class="container-fluid">
            <div class="hh-nav">
                <div class="container">
                    <div class="hh-logo">
                        <a href="{{ route('home') }}">
                            <img src="{{ url('frontend') }}/image/logo-tittle.png" alt="Home & House"
                                style="cursor: pointer;">
                        </a>

                    </div>
                    <button class="navbar-toggler" type="button" data-target="#myNav" id="myToggler">
                        <img src="{{ url('frontend') }}/image/menu-button.png" alt="menu">
                    </button>
                    <div class="" id="myNavToggler">

                        <div class=" navbar-collapse navbar-expand-sm" id="myNav">
                            <div class="hh-logo-mobile" style="display: none;">
                                <img src="{{ url('frontend') }}/image/logo-tittle.png" alt="Home & House"
                                    onclick="location.href={{ route('home') }}" style="cursor: pointer;">
                            </div>

                            <ul class="nav navbar-expad-sm">
                                @foreach ($menu as $item)
                                    <li class=" @if (Route::current()->getName() ===
                                    $item['name']) nav-item-active @else nav-item @endif">
                                        <a class="nav-link" href="{{ $item['href'] }}">{{ $item['title'] }}</a>
                                        <div class="nav-sticky"></div>
                                    </li>
                                @endforeach


                            </ul>
                        </div>
                    </div>

                    <div class="search">
                        <div class="icon">
                            <img class="search-btn" src="{{ url('frontend') }}/image/search-btn.png" alt="" id="we">
                        </div>
                    </div>
                </div>
                <div class="d-play-none search-box" id="search-input" style="transition: .3s;">
                    <input type="text" class="search-input">
                    <i class="fas fa-search"></i>

                </div>
            </div>

        </div>

    </nav>

</section>

<div class="button-sticky d-play-none animate__animated animate__pulse" onclick="location.href='#sc-home'">
    <i class="fas fa-angle-double-up"></i>
</div>
