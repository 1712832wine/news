<div class="container-fluid">
    <div class="footer">
        <div class="container">
            <a href="{{ route('home') }}">
                <img class="footer-logo" src="{{ url('frontend') }}/image/logo-tittle.png" alt="Home & House">
            </a>

            <div class="footer-info">
                <div class="footer-left">
                    <div class="footer-left-line">
                        <img src="{{ url('frontend') }}/image/home.png" alt="" class="icon-1">
                        <p style="font-weight: bold;">CÔNG TY TNHH THIẾT KẾ NỘI THẤT VÀ XÂY DỰNG HOME&HOUSE</p>
                    </div>
                    <div class="footer-left-line">
                        <img src="{{ url('frontend') }}/image/point.png" alt="" class="icon-2">
                        <p>Phạm Văn Xảo, Phường Phú Thọ Hoà, Quận Tân Phú, Thành phố Hồ Chí Minh</p>
                    </div>
                </div>

                <div class="footer-center">
                    <div class="footer-left-line">
                        <img src="{{ url('frontend') }}/image/phone.png" alt="" class="icon-3">
                        <p>0915490779</p>
                    </div>
                    <div class="footer-left-line">
                        <img src="{{ url('frontend') }}/image/mail.png" alt="" class="icon-4">
                        <p>Decorhomehouse@gmail.com</p>
                    </div>
                    <div class="footer-left-line">
                        <img src="{{ url('frontend') }}/image/word.png" alt="" class="icon-5">
                        <p>http://homehousedecor.vn</p>
                    </div>
                    <div class="footer-right footer-mobile" style="display: none;">
                        <div class="footer-contact">
                            <input type="text" placeholder="Decorhomehouse@gmail.com" style="width: 14.375vw;">
                            <button type="submit">
                                <p>Gửi</p>
                            </button>
                        </div>
                        <div class="footer-icon">
                            <a class="social-icon" href="">
                                <!-- <img src="frontend/image/Group 665h.png" alt="facebook"> -->
                                <img id="" src="{{ url('frontend') }}/image/home.png" alt="facebook">
                            </a>
                            <a class="social-icon" href=""><img id="" src="{{ url('frontend') }}/image/twish.png"
                                    alt="twitter"></a>
                            <a class="social-icon" href=""><img id="" src="{{ url('frontend') }}/image/youtube.png"
                                    alt="youtube" style="transform: scale(1.23);"></a>
                        </div>
                    </div>
                </div>
                <div class="footer-right">
                    <div class="footer-contact">
                        <input type="text" placeholder="Decorhomehouse@gmail.com" style="width: 14.375vw;">
                        <button type="submit">
                            <p>Gửi</p>
                        </button>
                    </div>
                    <div class="footer-icon">
                        <a class="social-icon" href="#">
                            <!-- <img src=rontend/image/Group 665h.png" alt="facebook"> -->
                            <img id="img1" src="{{ url('frontend') }}/image/facebook.png" alt="facebook">
                        </a>
                        <a class="social-icon" href="#">
                            <img id="img2" src="{{ url('frontend') }}/image/twish.png" alt="twitter">
                        </a>
                        <a class="social-icon" href="#">
                            <img id="img3" src="{{ url('frontend') }}/image/youtube.png" alt="youtube"
                                style="transform: scale(1.23);">
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="footer-end">
            <div class="container">
                <p>Copyrigh Smart Digi-tech</p>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

<script type="text/javascript" src="{{ url('frontend') }}/js/slick.min.js"></script>
<script type="text/javascript" src="{{ url('frontend') }}/js/java.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"
    integrity="sha512-uURl+ZXMBrF4AwGaWmEetzrd+J5/8NRkWAvJx5sbPSSuOb0bZLqf+tOzniObO00BjHa/dD7gub9oCGMLPQHtQA=="
    crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.css"
    integrity="sha512-nNlU0WK2QfKsuEmdcTwkeh+lhGs6uyOxuUs+n+0oXSYDok5qy0EI0lt01ZynHq6+p/tbgpZ7P+yUb+r71wqdXg=="
    crossorigin="anonymous" />

<script>
    $(document).ready(function() {

        $(".container a img").each(function() {
            /\.(?:jpg|jpeg|gif|png|webp)$/i.test($(this).parent("a").attr("href")) && ($(this)
                .parent(
                    "a").attr("data-src", $(this).parent("a").attr("href")), $(this).parent("a")
                .attr("data-fancybox", "gallery"))
        }), $('[data-fancybox="gallery"]').fancybox({
            transitionEffect: "slide",
            buttons: ["zoom", "slideShow", "fullScreen", "download", "thumbs", "close"]
        })
    });

</script>
<script>
    $('.carousel').carousel({
        interval: 2000
    })

</script>



<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" />
<link rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.green.min.css" />
<script>
    jQuery(document).ready(function($) {
        $('.owl-carousel').owlCarousel({
            loop: false,
            margin: 10,
            nav: false,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 2
                },
                1000: {
                    items: 3
                }
            }
        })
    })

</script>
<style>
    .owl-carousel.owl-theme{

     }
     .owl-carousel.owl-theme.owl-hidden{

     }
     .owl-carousel {
        display: block;
     }
     .news-info .news-link {
     }
    </style>
