@php
$count = count($articles);
$number_slide = ceil($count / 3);
$vt = 0;
@endphp
<div class="owl-carousel owl-theme">
    @for ($i = 0; $i < $count; $i++)
        <div class="item news-link">
            <a href="{{ route('newsdetail', [$articles[$i]['slug']]) }}">
                <div class="image">
                    @if (explode(',', $articles[$i]['image'])[0] !== '')
                        <img src="{{ url('storage/Articles') }}/{{ explode(',', $articles[$i]['image'])[0] }}"
                            alt="">
                    @endif
                </div>
            </a>
            <div class="text">
                <a href="{{ route('newsdetail', [$articles[$i]['slug']]) }}">
                    <p class="bold">{{ $articles[$i]['title'] }}</p>
                </a>
                <p class="time">{{ $articles[$i]['created_at'] }}</p>
                <div class="content" style="max-height: 41vh;
                        overflow: hidden; font-size: 0.73vw;">
                    {!! $articles[$i]['content'] !!}</div>
                <a href="{{ route('newsdetail', [$articles[$i]['slug']]) }}">
                    xem thêm
                </a>
            </div>
        </div>
    @endfor
</div>

<style>
    .owl-carousel.owl-theme{
        margin-right: 50px;

        display: flex;
     }
     .news-info .news-link {
        margin-right: 50px;
         }
    </style>
