<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{{ url('frontend') }}/css/home-housecss.css">
    <link rel="stylesheet" type="text/css" href="{{ url('frontend') }}/css/introducecss.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
    <link
        href="https://fonts.googleapis.com/css2?family=Bai+Jamjuree:ital,wght@0,200;0,300;0,500;0,600;1,200;1,300;1,400;1,500;1,600;1,700&display=swap"
        rel="stylesheet">
    <link rel="icon" type="image/png" href="{{ url('frontend') }}/image/logo.png" />
    <title>Home&House</title>
</head>

<body>
    @include('frontend.component.header')
    <!-- ============================================================= START HEADER ======================================================================================== -->
    @foreach ($articles as $article)
        <div class="container-fluid">
            @switch($article['template'])
                @case('template 1')
                    <div class="introduce-info-1">
                        <div class="container">
                            @if ($article['image'] !== '')
                                <div class="info-1-img">
                                    <img src="{{ url('storage/Articles') }}/{{ explode(',', $article['image'])[0] }}"
                                        alt="">
                                    <img class="img-mobile"
                                        src="{{ url('storage/Articles') }}/{{ explode(',', $article['image'])[1] }}"
                                        alt="">
                                </div>
                            @endif

                            <div class="info-1-tittle">
                                <div class="tittle-mobile">
                                    <h2>{{ $article['tags'] }}</h2>
                                    <h4>{{ $article['title'] }}</h4>
                                    <div class="sticky"></div>
                                </div>
                                <p>{!! $article['content'] !!}</p>
                            </div>
                        </div>
                    </div>

                @break
                @case('template 2')
                    <div class="introduce-info-2">
                        <div class="container">
                            <div class="info-2-tittle">
                                <div class="tittle-mobile">
                                    <h2>{{ $article['tags'] }}</h2>
                                    <h4>{{ $article['title'] }}</h4>
                                    <div class="sticky"></div>
                                </div>
                                <p>{!! $article['content'] !!}</p>
                            </div>
                            <div class="info-2-img">
                                <img src="{{ url('storage/Articles') }}/{{ explode(',', $article['image'])[0] }}"
                                    alt="">
                            </div>
                        </div>
                    </div>
                @break
                @default
                    <div class="introduce-info-3">
                        <div class="info-3-tittle">
                            <div class="tittle-mobile">
                                <h2>{{ $article['tags'] }}</h2>
                                <h4>{{ $article['title'] }}</h4>
                                <div class="sticky"></div>
                            </div>
                            <p>{!! $article['content'] !!}</p>
                        </div>
                    </div>
            @endswitch
        </div>
    @endforeach
    <div class="container-fluid">
        <div class="introduce-img">
            @foreach ($albums as $album_index => $album)
                <div class="introduce-img-line{{ ($album_index % 2) + 1 }}">
                    <div class="img-hover left">
                        <img src="{{ url('storage') }}/photos/{{ $lists[$album_index][0]['url'] }}" alt=""
                            style="height:100%">
                        <img src="{{ url('storage') }}/photos/{{ $lists[$album_index][0]['url'] }}" alt=""
                            class="img-mobile">
                        <div class="div-hover">
                            <h4>PHONG CÁCH</h4>
                            <h2>{{ $lists[$album_index][0]['content'] }}</h2>
                        </div>
                    </div>
                    <div class="img-hover right">
                        <img src="{{ url('storage') }}/photos/{{ $lists[$album_index][1]['url'] }}" alt=""
                            style="height:100%">
                        <img src="{{ url('storage') }}/photos/{{ $lists[$album_index][1]['url'] }}" alt=""
                            class="img-mobile">
                        <div class="div-hover">
                            <h4>PHONG CÁCH</h4>
                            <h2>{{ $lists[$album_index][1]['content'] }}</h2>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

    <!-- ============================================================= END INFO =========================================================================================== -->

    <!-- ============================================================= START FOOTER ======================================================================================= -->
    @include('frontend.component.footer')
</body>

</html>
