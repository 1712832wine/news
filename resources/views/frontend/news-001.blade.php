<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{{ url('frontend') }}/css/home-housecss.css">
    <link rel="stylesheet" type="text/css" href="{{ url('frontend') }}/css/news-001css.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
    <link
        href="https://fonts.googleapis.com/css2?family=Bai+Jamjuree:ital,wght@0,200;0,300;0,500;0,600;1,200;1,300;1,400;1,500;1,600;1,700&display=swap"
        rel="stylesheet">
    <link rel="icon" type="image/png" href="{{ url('frontend') }}/image/logo.png" />
    <title>Home&House</title>
</head>

<body>
    @include('frontend.component.header')
    <!-- ============================================================= START INFO ======================================================================================== -->
    <div class="container-fluid">
        <div class="rule-tittle">
            <div class="container">
                <div class="tittle">
                    {{-- <h4>3 nguyên tắc</h4> --}}
                    <h3>{{ $article['title'] }}</h3>
                    <div class="sticky"></div>
                    @if (count(json_decode($article['blocks_content'])) > 0)
                        {!! json_decode($article['blocks_content'])[0] !!}
                    @endif
                </div>

                <div class="image">
                    @if (count(explode(',', $article['image'])) > 0 && explode(',', $article['image'])[0] !== '')
                        <img src="{{ url('storage/Articles') }}/{{ explode(',', $article['image'])[0] }}" alt="">
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="rule-line">
            @if (count(json_decode($article['blocks_content'])) >= 2)
                {!! json_decode($article['blocks_content'])[1] !!}
            @endif
        </div>
    </div>
    <div class="container">
        <div class="rule-link">
            @if (count(json_decode($article['blocks_content'])) >= 3)
                {!! json_decode($article['blocks_content'])[2] !!}
            @endif

        </div>
    </div>
    <!-- ============================================================= END INFO =========================================================================================== -->

    <!-- ============================================================= START FOOTER ======================================================================================= -->
    @include('frontend.component.footer')
</body>

</html>
