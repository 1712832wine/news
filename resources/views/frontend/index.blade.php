<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/home-housecss.css') }}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
    <link
        href="https://fonts.googleapis.com/css2?family=Bai+Jamjuree:ital,wght@0,200;0,300;0,500;0,600;1,200;1,300;1,400;1,500;1,600;1,700&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css" />
    <link rel="icon" type="image/png" href="{{ asset('frontend/image/logo.png') }}" />
    <link rel="stylesheet" href="{{ asset('frontend/css/slick.css') }}">
    <title>Home&House</title>
</head>

<body>
    @include('frontend.component.header')
    <!-- ============================================================= START HEADER ======================================================================================== -->
    <div class="container-fluid ">
        <div class="hh-header">
            <div class="container">
                <div class="hh-tittle">
                    <h2>HOME & HOUSE</h2>
                    <H5>INTERIOR DESIGN AND CONSTRUCTION
                        COMPANY LIMITED</H5>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================= END HEADER ========================================================================================= -->
    <!-- ============================================================= START INFO ========================================================================================= -->
    @if ($introduction)
        <div class="container-fluid">
            <div class="hh-introduce">
                <div class="tittle">
                    <h4>{{ $introduction['title'] }}</h4>
                    <div class="sticky"></div>
                </div>
                <div class="info">
                    <img src="{{ url('frontend') }}/image/font.png" alt="">
                    {!! $introduction['content'] !!}
                    <img src="{{ url('frontend') }}/image/font.png" alt=""
                        style="transform: rotate(180deg) translateY(40%);">
                </div>
            </div>
        </div>
    @endif


    @foreach ($articles as $article)
        <div class="container-fluid">
            @switch($article['template'])
                @case('template 1')
                    <div class="hh-introduce-2">
                        <div class="container">
                            <div class="info">
                                <img class="img-mobile"
                                    src="{{ url('storage/Articles') }}/{{ explode(',', $article['image'])[0] }}" alt="">
                                <h4>{{ $article['title'] }}</h4>
                                <div class="sticky"></div>
                                {!! $article['content'] !!}
                                <img src="{{ url('storage/Articles') }}/{{ explode(',', $article['image'])[1] }}"
                                    alt="">
                            </div>
                        </div>
                    </div>
                @break
                @case('template 2')
                    <div class="hh-process">
                        <div class="container">
                            <div class="process-img">
                                <img src="{{ url('storage/Articles') }}/{{ explode(',', $article['image'])[0] }}"
                                    alt="">
                                <img class="img-mobile"
                                    src="{{ url('storage/Articles') }}/{{ explode(',', $article['image'])[1] }}"
                                    alt="">

                            </div>
                            <div class="info">
                                <h4>{{ $article['title'] }}</h4>
                                <div class="sticky"></div>
                                {!! $article['content'] !!}
                            </div>
                        </div>
                    </div>
                @break
                @default

            @endswitch

        </div>
    @endforeach

    <div class="container-fluid">
        <div class="hh-recruitment">
            <div class="tittle">
                <h4>{{ $recruitment['title'] }}</h4>
                <div class="sticky"></div>
            </div>
            {!! $recruitment['content'] !!}
        </div>
    </div>

    <div class="container-fluid">
        <div class="hh-partner">
            <div class="container">
                <div class="partner-slick" id="partner">
                    @if ($carousel_items)
                        @foreach ($carousel_items as $item)
                            <div>
                                <img src="{{ url('storage') }}/photos/{{ $item['url'] }}" alt=""
                                    style="width: 4.5vw;">
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>

    <!-- ============================================================= END INFO =========================================================================================== -->
    @include('frontend.component.footer')
</body>

</html>
