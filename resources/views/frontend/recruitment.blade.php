<div>

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="{{ url('frontend') }}/css/home-housecss.css">
        <link rel="stylesheet" type="text/css" href="{{ url('frontend') }}/css/recruitmentcss.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
        <link
            href="https://fonts.googleapis.com/css2?family=Bai+Jamjuree:ital,wght@0,200;0,300;0,500;0,600;1,200;1,300;1,400;1,500;1,600;1,700&display=swap"
            rel="stylesheet">
        <link rel="icon" type="image/png" href="./image/logo.png" />
        <title>Home&House</title>
    </head>

    <body>
        @include('frontend.component.header')
        <!-- ============================================================= START INFO ======================================================================================== -->

        <div class="contrainer-fluid recruitment-bg">
            <div class="container">
                @if ($short_desc)
                    <div class="recruitment-tittle">
                        <h4>{{ $short_desc['title'] }}</h4>
                        {!! $short_desc['content'] !!}
                    </div>
                @endif

                <div class="recruitment-info">
                    <div class="left">
                        <h4>Các vị trí</h4>
                        <p class="" style="color: #FECE59; font-size: 1.25vw;">Gửi thông tin liên hệ</p>
                    </div>
                    <div class="right">
                        <div class="recruitment-dropinfo">
                            @foreach ($recruitment as $index => $item)
                                <div class="dropinfo">
                                    <div class="dropinfo-tittle">
                                        <h4>{{ $item['title'] }}</h4>
                                        <img src="{{ url('frontend') }}/image/down.png"
                                            id="dropinfo-{{ $index }}" class="drop-info" alt=""
                                            style="transition: rotate(0deg);">
                                    </div>
                                    <div class="dropinfo-info" id="dropinfo-{{ $index }}-show">
                                        <p> {!! $item['content'] !!} </p>
                                    </div>
                                </div>
                            @endforeach

                            <div class="dropinfo  dropinfo-send">
                                <div class="dropinfo-tittle">
                                    <h4>Gửi thông tin để ứng tuyển</h4>
                                    <img src="{{ url('frontend') }}/image/down.png" id="dropinfo-4" class="drop-info"
                                        alt="" style="transition: rotate(0deg);">
                                </div>
                                <form class="dropinfo-info show" id="dropinfo-4-show" wire:submit.prevent="">
                                    <input type="text" placeholder="Nhập họ tên" wire:model.defer="name">
                                    <input type="text" placeholder="Nhập số điện thoại" wire:model.defer="phone">
                                    <input type="file" wire:model="file" style="padding:0 "
                                        id="upload{{ $iteration }}">
                                    <input type="submit" wire:click="submit()">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- ============================================================= END INFO =========================================================================================== -->

        <!-- ============================================================= START FOOTER ======================================================================================= -->
        @include('frontend.component.footer')
        <!-- ============================================================= END FOOTER ======================================================================================= -->

    </body>

</div>
