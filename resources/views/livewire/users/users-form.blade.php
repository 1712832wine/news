@php
$title = 'users';
$title2 = 'user';
@endphp
<main class="main pt-2">
    @include('livewire.component.header-form',[$title, $type])
    <div class="container-fluid animated fadeIn">
        <div class="row">
            <div class="col-md-8 bold-labels">
                <!-- Default box -->
                <form>
                    <div class="card">
                        <div class="card-body row">


                            <!-- text input -->
                            <div class="form-group col-sm-12 required">
                                <label for="name">Name</label>

                                <input type="text" id="name" class="form-control" wire:model.defer="name">
                                @error('name') <span class="error text-danger">{{ $message }}</span> @enderror
                            </div>

                            @if ($type === 'create')
                                <!-- text input -->
                                <div class="form-group col-sm-12 required"> <label for="mail">Email</label>
                                    <input type="email" id="mail" class="form-control" wire:model.defer="email">
                                    @error('email') <span class="error text-danger">{{ $message }}</span> @enderror
                                </div>

                                <!-- password -->
                                <div class="form-group col-sm-12 required">
                                    <label for="password">Password</label>
                                    <input type="password" id="password" class="form-control"
                                        autocomplete="new-password" wire:model.defer="password">
                                    @error('password') <span class="error text-danger">{{ $message }}</span>
                                    @enderror

                                </div>
                                <!-- password -->
                                <div class="form-group col-sm-12 required">
                                    <label for="password_c">Password Confirmation</label>
                                    <input id="password_c" type="password" autocomplete="off" class="form-control"
                                        wire:model.defer="password_confirmation">
                                    @error('password_confirmation')
                                        <span class="error text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            @endif


                            <!-- dependencyJson -->

                            <div class="form-group col-sm-12 checklist_dependency">
                                <label>User & Role Permissions</label>

                                <div class="container">

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label>Roles</label>
                                        </div>
                                    </div>

                                    <div class="row">

                                        <div class="hidden_fields_primary">
                                        </div>
                                        @foreach ($Role_values as $Role_value)
                                            <div class="col-sm-4">
                                                <div class="checkbox">
                                                    <label class="font-weight-normal">
                                                        <input type="checkbox"
                                                            number_columns="{{ count($Role_values) }}"
                                                            value="{{ $Role_value->name }}" wire:model.lazy="roles">
                                                        {{ $Role_value->name }}
                                                    </label>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label>Permission</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="hidden_fields_secondary">
                                        </div>
                                        @foreach ($Permission_values as $Permission_value)
                                            <div class="col-sm-4">
                                                <div class="checkbox">
                                                    <label class="font-weight-normal">
                                                        <input type="checkbox"
                                                            number_columns="{{ count($Permission_values) }}"
                                                            value="{{ $Permission_value->name }}"
                                                            wire:model.defer="permissions" @if (in_array($Permission_value->name, $isRole)) disabled @endif>
                                                        {{ $Permission_value->name }}
                                                    </label>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div><!-- /.container -->
                            </div>
                        </div>
                    </div>
                    @include('livewire.component.saveaction-form')
                </form>
            </div>
        </div>
    </div>
    <style>
        input:disabled {
            color: rgb(84, 84, 84);
        }

    </style>
</main>
