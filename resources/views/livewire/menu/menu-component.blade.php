@if ($isOpen)
    @include('livewire.menu.menu-form')
@else
    @php
        $breadcrumb = [
            'items' => [
                [
                    'href' => route('dashboard'),
                    'title' => 'dashboard',
                ],
                [
                    'href' => route('menu'),
                    'title' => 'menu',
                ],
            ],
            'active' => 'List',
        ];
        $header = [
            'title' => 'menu items',
            'reset' => route('menu'),
        ];
        $button = 'Add menu item';
        $table_header = ['Label', 'Parent', 'Actions'];
        $table_body = ['label', 'parent'];

    @endphp
    @include('livewire.component.main-component',[$breadcrumb, $header, $button, $table_header, $table_body,$table_actions=[],$selectBox=false])
@endif
