@switch($isOpen)
    @case(1)
        @include('livewire.albums.albums-form')
    @break
    @case(2)
        @include('livewire.albums.asset-form')
    @break
    @default
        @php
        $breadcrumb = [
            'items' => [
                [
                    'href' => route('dashboard'),
                    'title' => 'dashboard',
                ],
                [
                    'href' => route('albums'),
                    'title' => 'albums',
                ],
            ],
            'active' => 'List',
        ];
        $header = [
            'title' => 'albums',
            'reset' => route('albums'),
        ];
        $button = 'Add albums';
        $table_header = ['name', 'type', 'status', 'Actions'];
        $table_body = ['name', 'type', 'status'];
        @endphp
        @include('livewire.component.main-component',[$breadcrumb, $header, $button, $table_header, $table_body,$table_actions=[],$selectBox=false])
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"
            integrity="sha256-T0Vest3yCU7pafRw9r+settMBX6JkKN06dqBnpQ8d30=" crossorigin="anonymous"></script>
        <script>
            // var fixHelperModified = function(e, tr) {
            //         var $originals = tr.children();
            //         var $helper = tr.clone();
            //         $helper.children().each(function(index) {
            //             $(this).width($originals.eq(index).width())
            //         });
            //         return $helper;
            //     },
            //     updateIndex = function(e, ui) {
            //         // console.log("ui", ui.item.attr("key"))
            //         $('td.index', ui.item.parent()).each(function(i) {
            //             $(this).html(i + 1);
            //         });
            //         $('input[type=text]', ui.item.parent()).each(function(i) {
            //             $(this).val(i + 1);
            //         });
            //     };

            // $("#myTable tbody").sortable({
            //     helper: fixHelperModified,
            //     stop: updateIndex,
            // }).disableSelection();
            $("tbody").sortable({
                distance: 5,
                delay: 100,
                opacity: 0.6,
                placeholder: 'highlight',
                cursor: 'move',
                update: function(e, ui) {
                    var sortData = $("#myTable tbody").sortable('toArray', {
                        attribute: 'data-id'
                    })
                    @this.confirmChangePriority(sortData);
                }
            });

        </script>

        <style>
            .dragged {
                position: absolute;
                opacity: 0.5;
                z-index: 2000;
                background: black;
            }

        </style>
@endswitch
