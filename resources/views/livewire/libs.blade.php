<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    {{-- Styles --}}
    <link rel="stylesheet" type="text/css"
        href="https://demo.backpackforlaravel.com/packages/backpack/base/css/bundle.css?v=4.1.40@ecbe7676535d3c9121227329d62cad9978d29fee">
    <link rel="stylesheet" type="text/css"
        href="https://demo.backpackforlaravel.com/packages/source-sans-pro/source-sans-pro.css?v=4.1.40@ecbe7676535d3c9121227329d62cad9978d29fee">
    <link rel="stylesheet" type="text/css"
        href="https://demo.backpackforlaravel.com/packages/line-awesome/css/line-awesome.min.css?v=4.1.40@ecbe7676535d3c9121227329d62cad9978d29fee">
    <link rel="stylesheet" type="text/css"
        href="https://demo.backpackforlaravel.com/packages/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css"
        href="https://demo.backpackforlaravel.com/packages/datatables.net-fixedheader-bs4/css/fixedHeader.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css"
        href="https://demo.backpackforlaravel.com/packages/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet"
        href="https://demo.backpackforlaravel.com/packages/backpack/crud/css/crud.css?v=4.1.40@ecbe7676535d3c9121227329d62cad9978d29fee">
    <link rel="stylesheet"
        href="https://demo.backpackforlaravel.com/packages/backpack/crud/css/form.css?v=4.1.40@ecbe7676535d3c9121227329d62cad9978d29fee">
    <link rel="stylesheet"
        href="https://demo.backpackforlaravel.com/packages/backpack/crud/css/list.css?v=4.1.40@ecbe7676535d3c9121227329d62cad9978d29fee">
    <link rel="stylesheet"
        href="https://unpkg.com/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.min.css">
    <link rel="stylesheet" href="https://unpkg.com/filepond/dist/filepond.min.css">
    @livewireStyles

    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}" defer></script>
    {{-- Scripts --}}
    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.8.2/dist/alpine.min.js" defer></script>
    <script type="text/javascript"
        src="https://demo.backpackforlaravel.com/packages/backpack/base/js/bundle.js?v=4.1.40@ecbe7676535d3c9121227329d62cad9978d29fee">
    </script>
    <script type="text/javascript"
        src="https://demo.backpackforlaravel.com/packages/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
</head>
