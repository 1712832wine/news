@php
$title = 'Assets';
$title2 = 'asset';
$type = 'edit';
@endphp
<main class="main pt-2">
    @include('livewire.component.header-form',[$title, $type])
    <div class="container-fluid animated fadeIn">
        <div class="row">
            <div class="col-md-8 bold-labels">
                <!-- Default box -->
                <form>
                    <div class="card">
                        <div class="card-body row">
                            <div class="form-group col-sm-12">
                                <label>This Images</label>
                                <div class="w-1/2 p-2">
                                    <div class="w-full h-full border">
                                        <img class="" src="{{ asset('storage/photos/' . $a_url) }}">
                                    </div>
                                </div>
                                <!-- text input -->
                                <div class="form-group col-sm-12">
                                    <label for="name">Name</label>
                                    <input type="text" id="title" wire:model.defer="a_name" class="form-control">
                                </div>
                                <div class="form-group col-sm-12">
                                    <label for="short_desc">Short_desc</label>
                                    <input type="text" id="short_desc" wire:model.defer="a_short_desc"
                                        class="form-control">
                                </div>
                                <div class="form-group col-sm-12">
                                    <label for="content">Content</label>
                                    <input type="text" id="content" wire:model.defer="a_content" class="form-control">
                                </div>
                                <div class="form-group col-sm-12">
                                    <label for="type">Type</label>
                                    <input type="text" id="type" wire:model.defer="a_type" class="form-control">
                                </div>
                                <div class="form-group col-sm-12">
                                    <label for="url">URL</label>
                                    <input type="text" id="url" wire:model.defer="a_url" class="form-control">
                                </div>
                                <div class="form-group col-sm-12">
                                    <label for="priority">Priority</label>
                                    <input type="text" id="priority" wire:model.defer="a_priority" class="form-control">
                                </div>
                                <div class="form-group col-sm-12">
                                    <label for="status">Status</label>
                                    <input type="text" id="status" wire:model.defer="a_status" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    @include('livewire.component.saveaction-form')
                </form>
            </div>
        </div>
    </div>
</main>
