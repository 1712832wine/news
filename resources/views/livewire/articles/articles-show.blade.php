<main class="main pt-2">


    <nav aria-label="breadcrumb" class="d-none d-lg-block">
        <ol class="breadcrumb bg-transparent p-0 justify-content-end">
            <li class="breadcrumb-item text-capitalize"><a href="{{ route('dashboard') }}">Dashboard</a></li>
            <li class="breadcrumb-item text-capitalize"><a href="{{ route('articles') }}">articles</a></li>
            <li class="breadcrumb-item text-capitalize active" aria-current="page">Preview</li>
        </ol>
    </nav>


    <section class="container-fluid d-print-none">
        <a href="javascript: window.print();" class="btn float-right"><i class="la la-print"></i></a>
        <h2>
            <span class="text-capitalize">articles</span>
            <small>Preview article.</small>
            <small class="">
                <a href="" wire:click.prevent="closePreview()" class="font-sm">
                    <i class="la la-angle-double-left"></i>
                    Back to all <span>articles</span>
                </a>
            </small>
        </h2>
    </section>

    <div class="container-fluid animated fadeIn">


        <div class="row">
            <div class="col-md-8">

                <!-- Default box -->
                <div class="">
                    <div class="card no-padding no-border">
                        <table class="table table-striped mb-0">
                            <tbody>
                                <tr>
                                    <td>
                                        <strong>Content:</strong>
                                    </td>
                                    <td>
                                        <span>
                                            {{ $Article->content }}
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Date:</strong>
                                    </td>
                                    <td>
                                        <span data-order="1986-12-06 00:00:00">
                                            {{ $Article->date }}
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Featured:</strong>
                                    </td>
                                    <td>
                                        <span data-order="">
                                            {{ $Article->featured }}
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Image:</strong>
                                    </td>
                                    <td>
                                        <span>
                                            {{ $Article->image }}
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Slug:</strong>
                                    </td>
                                    <td>
                                        <span>
                                            {{ $Article->slug }}
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Status:</strong>
                                    </td>
                                    <td>
                                        <span>
                                            {{ $Article->status }}
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Title:</strong>
                                    </td>
                                    <td>
                                        <span>
                                            {{ $Article->title }}
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td><strong>Actions</strong></td>
                                    <td>
                                        <!-- Single edit button -->
                                        <a href="" wire.click.prevent="edit({{ $item_id }},'edit')"
                                            class="btn btn-sm btn-link">
                                            <i class="la la-edit"></i>Edit
                                        </a>

                                        <a href="" wire.click.prevent="confirmDelete({{ $item_id }})"
                                            class="btn btn-sm btn-link" data-button-type="delete">
                                            <i class="la la-trash"></i> Delete
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
