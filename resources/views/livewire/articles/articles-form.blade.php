@php
$title = 'articles';
$title2 = 'artile';
@endphp
<main class="main pt-2" id="article-form">
    @include('livewire.component.header-form',[$title, $type])
    <div class="container-fluid animated fadeIn">
        <div class="row">
            <div class="col bold-labels">
                <!-- Default box -->
                <form wire:submit.prevent="">
                    <div class="card">
                        <div class="card-body row">
                            <!-- text input -->
                            <div class="form-group col-sm-12 required">
                                <label for="title">Title</label>
                                <input type="text" id="title" name="title" wire:model.defer="title"
                                    class="form-control">
                                @error('title')
                                    <span class="error text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <!-- text input -->
                            <div class="form-group col-sm-12">
                                <label for="slug">Slug (URL)</label>
                                <input type="text" id="slug" wire:model.defer="slug" class="form-control">
                                <p class="help-block">Will be automatically generated from your title, if left empty.
                                </p>
                            </div>
                            <!--  relationship  -->
                            <div class="form-group col-sm-12">
                                <label for="category">Category</label>
                                <select id="category" class="form-control" wire:model.defer="category">
                                    <option value="" selected>_</option>
                                    @foreach ($categories as $index => $item)
                                        <option value="{{ $item->name }}">{{ $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="template">Template</label>
                                <select id="template" class="form-control" wire:model.defer="template">
                                    <option value="" selected>_</option>
                                    <option value="template 1">template 1</option>
                                    <option value="template 2">template 2</option>
                                </select>
                            </div>
                            <!-- html5 date input -->
                            <div class="form-group col-sm-12 required">
                                <label for="date">Date</label>
                                <input id="date" wire:model.defer="date" class="form-control" type="date">
                                @error('date')
                                    <span class="error text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <!-- CKeditor -->
                            <div class="form-group col-sm-12 required" wire:ignore>
                                <label for="content">Content</label>
                                <textarea id="content" name="content" class="form-control" data-note="@this" rows="4"
                                    wire:model.defer="content"></textarea>
                                @error('content')
                                    <span class="error text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <!-- browse server input -->
                            <div class="form-group col-sm-12">
                                <label for="image">Images</label>
                                <input type="file" class="form-control mb-2" id="image" wire:model.defer="images"
                                    multiple />
                            </div>
                            @if ($images !== false)
                                <div class="row col-sm-12 form-group" style="margin: auto;">
                                    <label for="" class="w-100"> Photo Preview</label>
                                    @foreach ($images as $image)
                                        <div class="col-12 col-md-3">
                                            <div class="card">
                                                <img class="card-img-top" style="width:100%; height: 150px;"
                                                    src="{{ $image->temporaryUrl() }}" />
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            @endif

                            @if ($imageList && $imageList[0] !== '')
                                <div class="row col-sm-12 form-group" style="margin: auto;">
                                    <label for="" class="w-100">Photos of this article</label>
                                    @foreach ($imageList as $index => $image)
                                        <div class="col-12 col-md-3">
                                            <div class="card mycontainer">
                                                <img class="card-img-top image" style="width:100%; height: 150px;"
                                                    src="{{ asset('storage/Articles/' . $image) }}">
                                                <div class="middle">
                                                    <button type="button"
                                                        class="btn btn-lg btn-danger rounded-pill ml-1"
                                                        wire:click="confirmDeleteAsset({{ $index }})">
                                                        <i class="la la-trash"> </i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            @endif



                            <!--  relationship  -->
                            <div class="form-group col-sm-12">
                                <label for="tags">Tags</label>
                                <select id="tags" class="form-control" wire:model.defer="tags">
                                    <option value="" selected>_</option>
                                    @foreach ($tags as $index => $item)
                                        <option value="{{ $item->name }}">{{ $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <!-- enum -->
                            <div class="form-group col-sm-12 required">
                                <label for="status">Status</label>
                                <select id="status" class="form-control" wire:model.defer="status">
                                    <option value="">_</option>
                                    <option value="PUBLISHED" selected>PUBLISHED</option>
                                    <option value="DRAFT">DRAFT</option>
                                </select>
                                @error('status')
                                    <span class="error text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <!-- checkbox field -->
                            <div class="form-group col-sm-12">
                                <div class="checkbox">
                                    <input type="checkbox" id="featured" wire:model.defer="featured">
                                    <label class="form-check-label font-weight-normal" for="featured">
                                        Featured item
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="saveActions" class="form-group">
                        <input type="hidden" name="save_action" value="save_and_back">
                        <div class="btn-group" role="group">
                            <button type="submit" id="submit" class="btn btn-success">
                                <span class="la la-save" role="presentation" aria-hidden="true"></span>
                                &nbsp; <span data-value="save_and_back">Save and back</span>
                            </button>
                        </div>
                        <a href="" wire:click.prevent="closeModal()" class="btn btn-default">
                            <span class="la la-ban"></span>
                            &nbsp;Cancel
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <style>
        #article-form .ck-editor__main ul {
            list-style-position: inside;
        }

        #article-form .ck-editor__main li {
            list-style-type: disc;
        }

        #article-form .ck-editor__main ol {
            list-style: none;
            counter-reset: my-awesome-counter;
        }

        #article-form .ck-editor__main ol li {
            counter-increment: my-awesome-counter;
        }

        #article-form .ck-editor__main ol li::before {
            content: counter(my-awesome-counter) ". ";
            font-weight: bold;
        }

        #article-form .mycontainer {
            position: relative;
        }

        #article-form .image {
            opacity: 1;
            display: block;
            transition: .5s ease;
            backface-visibility: hidden;
            width: 100%;
            height: 100%;
        }

        #article-form .middle {
            width: 100%;
            display: flex;
            justify-content: center;
            transition: .5s ease;
            display: none;
            position: absolute;
            text-align: center;
            opacity: 0;
        }

        #article-form .mycontainer:hover .image {
            opacity: 0.3;
        }

        #article-form .mycontainer:hover .middle {
            display: block;
            transform: translate(-50%, -50%);
            top: 50%;
            left: 50%;
            opacity: 0.8;
        }

    </style>
    <script>
        CKEDITOR.replace('content', {
            filebrowserBrowseUrl: '/file_manager/ckeditor',
            filebrowserImageBrowseUrl: '/file_manager/ckeditor',
            filebrowserFlashBrowseUrl: '/file_manager/ckeditor',
            filebrowserUploadUrl: '/file_manager/ckeditor',
            filebrowserImageUploadUrl: '/file_manager/ckeditor',
            filebrowserFlashUploadUrl: '/file_manager/ckeditor',
            heading: {
                options: [{
                        model: 'paragraph',
                        title: 'Paragraph',
                        class: 'ck-heading_paragraph'
                    },
                    {
                        model: 'heading1',
                        view: 'h1',
                        title: 'Heading 1',
                        class: 'ck-heading_heading1'
                    },
                    {
                        model: 'heading2',
                        view: 'h2',
                        title: 'Heading 2',
                        class: 'ck-heading_heading2'
                    },
                    {
                        model: 'heading3',
                        view: 'h3',
                        title: 'Heading 3',
                        class: 'ck-heading_heading3'
                    },
                    {
                        model: 'heading4',
                        view: 'h4',
                        title: 'Heading 4',
                        class: 'ck-heading_heading4'
                    },
                    {
                        model: 'heading5',
                        view: 'h5',
                        title: 'Heading 5',
                        class: 'ck-heading_heading5'
                    },
                    {
                        model: 'heading6',
                        view: 'h6',
                        title: 'Heading 6',
                        class: 'ck-heading_heading6'
                    }
                ]
            }
        })
        // .then(editor => {
        document.querySelector('#submit').addEventListener(('click'), () => {
            @this.content = CKEDITOR.instances.content.getData();
            @this.saveAndBack();
        })
        // })
        // .catch(error => {
        //     console.error(error);
        // });

    </script>


</main>
