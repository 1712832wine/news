  @if ($isOpen)
      @include('livewire.articles.articles-form',['categories' => App\Models\Category::all(),'tags'
      =>App\Models\Tag::all()])
  @else
      @php
          $breadcrumb = [
              'items' => [
                  [
                      'href' => route('dashboard'),
                      'title' => 'dashboard',
                  ],
                  [
                      'href' => route('articles'),
                      'title' => 'Articles',
                  ],
              ],
              'active' => 'List',
          ];
          $header = [
              'title' => 'Articles',
              'reset' => route('articles'),
          ];
          $button = 'Add article';
          $table_header = ['Title', 'Category', 'Status', 'Actions'];
          $table_body = ['title', 'category', 'status'];
          $table_actions = ['clone'];

      @endphp
      @include('livewire.component.main-component',[$breadcrumb, $header, $button, $table_header, $table_body,$table_actions,$table_actions=['clone'],$selectBox=true])
  @endif
