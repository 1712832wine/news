<div>
    @php
        $breadcrumb = [
            'items' => [
                [
                    'href' => route('dashboard'),
                    'title' => 'dashboard',
                ],
                [
                    'href' => route('recruitments'),
                    'title' => 'recruitments',
                ],
            ],
            'active' => 'List',
        ];
        $header = [
            'title' => 'Recruitments',
            'reset' => route('recruitments'),
        ];
        $table_header = ['Name', 'Phone', 'Actions'];
        $table_body = ['name', 'phone'];
    @endphp
    <main class="main pt-2">
        @include('livewire.component.breadcrumb', $breadcrumb)
        <div class="container-fluid mb-2">
            <div class="row">
                <h2 class="col-sm-6">
                    <span class="text-capitalize">{{ $header['title'] }}</span>
                    <small id="datatable_info_stack" class="animated fadeIn" style="display: inline-flex;">
                        <div class="dataTables_info">Showing
                            {{ $list->firstItem() }} to {{ $list->lastItem() }} of
                            {{ $list->total() }}
                            entries.</div>
                        <a href="{{ $header['reset'] }}" class="ml-1">Reset</a>
                    </small>
                </h2>
                <div class="col-sm-6">
                    <div id="datatable_search_stack" class="mt-sm-0 mt-2 d-print-none">
                        <div id="crudTable_filter" class="dataTables_filter">
                            <label>
                                <input type="search" class="form-control" placeholder="Search..."
                                    wire:model.debounce.250ms="search">
                            </label>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="container-fluid animated fadeIn">
            <!-- Default box -->
            <div class="row">
                <!-- THE ACTUAL CONTENT -->
                <div class="col-md-12">
                    <div class="dataTables_wrapper dt-bootstrap4">
                        <div class="row">
                            <div class="col-sm-12">
                                <table
                                    class="bg-white table table-striped table-hover nowrap rounded shadow-xs border-xs mt-2 dataTable dtr-inline">
                                    <thead>
                                        <tr role="row">
                                            <th class="sorting_disabled" rowspan="1" colspan="1">
                                                <input type="checkbox" class="crud_bulk_actions_main_checkbox"
                                                    style="width: 16px; height: 16px;" wire:model="selectAll">
                                            </th>
                                            @foreach ($table_header as $item)
                                                <th rowspan="1" colspan="1">
                                                    {{ $item }}
                                                </th>
                                            @endforeach
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($list as $item)
                                        <tr class="@if ($item->id % 2) odd @else
                                                even @endif">
                                                <td>
                                                    <span>
                                                        <input type="checkbox" class="crud_bulk_actions_row_checkbox"
                                                            wire:model.defer="selected" value="{{ $item->id }}"
                                                            style="width: 16px; height: 16px; vertical-align: middle; margin-bottom: 2px;">
                                                    </span>
                                                </td>
                                                @foreach ($table_body as $col)
                                                    <td>
                                                        <span>
                                                            {{ $item->$col }}
                                                        </span>
                                                    </td>
                                                @endforeach
                                                <td>
                                                    <!-- Single edit button -->
                                                    <a href="" wire:click.prevent="download({{ $item->id }})"
                                                        class="btn btn-sm btn-link">
                                                        <i class="la la-download"></i> Download
                                                    </a>

                                                    <a href="" wire:click.prevent="confirmDelete({{ $item->id }})"
                                                        class="btn btn-sm btn-link" data-button-type="delete">
                                                        <i class="la la-trash"></i> Delete
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        @include('livewire.component.selected-action')
                        @include('livewire.component.pagination')
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
