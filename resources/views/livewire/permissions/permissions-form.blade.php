@php
$title = 'permissions';
$title2 = 'permission';
@endphp
<main class="main pt-2">
    @include('livewire.component.header-form',[$title, $type])
    <div class="container-fluid animated fadeIn">



        <div class="row">
            <div class="col-md-8 bold-labels">
                <!-- Default box -->


                <form>


                    <div class="card">
                        <div class="card-body row">
                            <!-- load the view from type and view_namespace attribute if set -->
                            <!-- text input -->
                            <div class="form-group col-sm-12 required" element="div"> <label>Name</label>
                                <input type="text" name="name" value="" class="form-control" wire:model.defer="name">
                                @error('name')
                                    <span class="error text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                        </div>
                    </div>






                    @include('livewire.component.saveaction-form')

                </form>
            </div>
        </div>



    </div>

</main>
