{{-- Breadcrumb --}}
<nav aria-label="breadcrumb" class="d-none d-lg-block">
    <ol class="breadcrumb bg-transparent p-0 justify-content-end">
        <li class="breadcrumb-item text-capitalize">
            <a href="{{ route('dashboard') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item text-capitalize">
            <a href="" wire:click.prevent="closeModal()">{{ $title }}</a>
        </li>
        <li class="breadcrumb-item text-capitalize active" aria-current="page">{{ $type }}</li>
    </ol>
</nav>
{{-- Return text --}}
<section class="container-fluid">
    <h2>
        <span class="text-capitalize">{{ $title }}</span>
        <small class="text-capitalize">{{ $type }} {{ $title2 }}.</small>

        <small>
            <a href="" wire:click.prevent="closeModal()" class="d-print-none font-sm">
                <i class="la la-angle-double-left"></i> Back to all <span>{{ $title }}</span>
            </a>
        </small>
    </h2>
</section>
