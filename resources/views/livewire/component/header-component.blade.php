<div class="container-fluid">
    <h2>
        <span class="text-capitalize">{{ $header['title'] }}</span>
        <small id="datatable_info_stack" class="animated fadeIn" style="display: inline-flex;">
            <div class="dataTables_info">Showing
                {{ $list->firstItem() }} to {{ $list->lastItem() }} of
                {{ $list->total() }}
                entries.</div>
            <a href="{{ $header['reset'] }}" class="ml-1">Reset</a>
        </small>
    </h2>
</div>
