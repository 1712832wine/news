<div class="row">
    <div class="col-sm-12">
        <table drag-root id="myTable"
            class="bg-white table table-striped table-hover nowrap rounded shadow-xs border-xs mt-2 dataTable dtr-inline">
            <thead>
                <tr role="row">
                    @if ($selectBox)
                    <th class="sorting_disabled" rowspan="1" colspan="1">
                        <input type="checkbox" class="crud_bulk_actions_main_checkbox"
                            style="width: 16px; height: 16px;" wire:model="selectAll">
                    </th>
                    @endif

                    @foreach ($table_header as $item)
                        <th rowspan="1" colspan="1">
                            {{ $item }}
                        </th>
                    @endforeach
                </tr>
            </thead>
            <tbody>
                @foreach ($list as $index => $item)
                    <tr data-id="{{ $item->id }}" drag-item draggable="true" class="@if ($index % 2) odd @else even @endif">
                        @if ($selectBox)

                        <td>
                            <span>
                                <input type="checkbox" class="crud_bulk_actions_row_checkbox"
                                    wire:model.defer="selected" value="{{ $item->id }}"
                                    style="width: 16px; height: 16px; vertical-align: middle; margin-bottom: 2px;">
                            </span>
                        </td>
                        @endif
                        @foreach ($table_body as $col)
                            <td>
                                <span>

                                    {{ @json_decode(@json_encode($item->$col)) }}

                                </span>
                            </td>
                        @endforeach
                        <td>
                            @foreach ($table_actions as $table_action)
                                <a href="" wire:click.prevent="{{ $table_action }}({{ $item->id }})"
                                    class="btn btn-sm btn-link capitalize">
                                    <i class="la la-edit"></i> {{ $table_action }}
                                </a>
                            @endforeach

                            <!-- Single edit button -->
                            <a href="" wire:click.prevent="edit({{ $item->id }},'edit')"
                                class="btn btn-sm btn-link">
                                <i class="la la-edit"></i> Edit
                            </a>

                            <a href="" wire:click.prevent="confirmDelete({{ $item->id }})"
                                class="btn btn-sm btn-link" data-button-type="delete">
                                <i class="la la-trash"></i> Delete
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
