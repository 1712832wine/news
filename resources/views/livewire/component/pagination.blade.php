<div class="row mt-2">
    <div class="col-sm-12 col-md-4 col-12">
        <div class="dataTables_length">
            <label>
                <select class="custom-select custom-select-sm form-control form-control-sm"
                    wire:model="pagination_size">
                    <option value="6">6</option>
                    <option value="10">10</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                    <option value="1000">All </option>
                </select>
                entries per page

            </label>
        </div>
    </div>
    <div class="col-sm-0 col-md-4 text-center"></div>
    <div class="col-sm-12 col-md-4 ">
        <div class="dataTables_paginate paging_simple_numbers" id="crudTable_paginate">
            <ul class="pagination">
                {{ $list->links() }}
            </ul>
        </div>
    </div>
</div>
