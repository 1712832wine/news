<div id="saveActions" class="form-group">
    <input type="hidden" name="save_action" value="save_and_back">
    <div class="btn-group" role="group">

        <button type="submit" id="submit" class="btn btn-success" wire:click.prevent="saveAndBack()">
            <span class="la la-save" role="presentation" aria-hidden="true"></span>
            &nbsp; <span data-value="save_and_back">Save and back</span>
        </button>
    </div>
    <a href="" wire:click.prevent="closeModal()" class="btn btn-default">
        <span class="la la-ban"></span>
        &nbsp;Cancel
    </a>
</div>
