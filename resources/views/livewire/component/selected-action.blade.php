<div id="bottom_buttons" class="d-print-none text-center text-sm-left">
    <button class="btn btn-sm btn-secondary bulk-button disabled pointer" wire:click="confirmDeleteSelected()">
        <i class="la la-trash"></i> Delete selected
    </button>
    <div id="datatable_button_stack" class="float-right text-right hidden-xs"></div>
</div>
