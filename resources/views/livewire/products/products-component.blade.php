@if ($isOpen)
    @include('livewire.products.products-form')
@else
    @php
        $breadcrumb = [
            'items' => [
                [
                    'href' => route('dashboard'),
                    'title' => 'dashboard',
                ],
                [
                    'href' => route('products'),
                    'title' => 'products',
                ],
            ],
            'active' => 'List',
        ];
        $header = [
            'title' => 'products',
            'reset' => route('products'),
        ];
        $button = 'Add product';
        $table_header = ['Name', 'Description', 'Actions'];
        $table_body = ['name', 'description'];
        $table_actions = ['clone'];

    @endphp
    <main class="main pt-2">
        @include('livewire.component.breadcrumb', $breadcrumb)
        @include('livewire.component.header-component', $header)
        <div class="container-fluid animated fadeIn">
            <!-- Default box -->
            <div class="row">
                <!-- THE ACTUAL CONTENT -->
                <div class="col-md-12">
                    @include('livewire.component.addbtn-and-searchbox', [$button])
                    <div class="dataTables_wrapper dt-bootstrap4">
                        @include('livewire.component.table',[$table_header, $table_body, $table_actions,$selectBox=true])
                        @include('livewire.component.selected-action')
                        @include('livewire.component.pagination')
                    </div>
                </div>
            </div>
        </div>
    </main>

@endif
