<div role="tabpanel" class="tab-pane active" id="tab_texts">

    <div class="row">


        <!-- text input -->
        <div class="form-group col-sm-12 required" element="div">
            <label for="name">Name</label>
            <input type="text" id="name" wire:model.defer="name" class="form-control">
            @error('name')
                <span class="error text-danger">{{ $message }}</span>
            @enderror
        </div>


        <!-- textarea -->
        <div class="form-group col-sm-12" element="div">
            <label for="description">Description</label>
            <textarea id="description" wire:model.defer="description" class="form-control"></textarea>
        </div>

        <!-- CKeditor -->
        <div class="form-group col-sm-12" wire:ignore>
            <label for="content">Details</label>
            <textarea id="content" name="content" class="form-control" data-note="@this" rows="4"
                wire:model.defer="details"></textarea>
        </div>


        <!-- Backpack Table Field Type -->
        <div class="form-group col-sm-12" element="div" style="background: #f5f5f5">
            <hr>
            <h4>Features</h4>
            <hr>
        </div>

        <div data-field-type="table" data-field-name="features" class="form-group col-12" element="div">
            <div class="array-container form-group">

                <table class="table table table-striped m-b-0 border">

                    <thead>
                        <tr>
                            <th style="font-weight: 600!important;">
                                Feature
                            </th>
                            <th style="font-weight: 600!important;">
                                Value
                            </th>
                            <th class="text-center"> </th>
                        </tr>
                    </thead>

                    <tbody class="table-striped items sortableOptions ui-sortable">
                        @foreach ($features as $index => $item)
                            <tr class="array-row" style="">
                                <td>
                                    <input class="form-control form-control" type="text" data-cell-name="item.name"
                                        wire:model.defer="features.{{ $index }}.name">
                                </td>
                                <td>
                                    <input class="form-control form-control" type="text" data-cell-name="item.desc"
                                        wire:model.defer="features.{{ $index }}.desc">
                                </td>

                                <td>
                                    <button class="btn btn-light removeItem" type="button" style="width:100%"
                                        wire:click="removeFeature({{ $index }})">
                                        <i class="la la-trash" role="presentation" aria-hidden="true"></i>
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>

                </table>

                <div class="array-controls btn-group m-t-10">
                    <button class="btn btn btn-light" type="button" wire:click="addFeature()">
                        <i class="la la-plus"></i> Add feature
                    </button>
                </div>
            </div>

        </div>

        <!-- Backpack Table Field Type -->
        <div class="form-group col-sm-12" element="div" style="background: #f5f5f5">
            <hr>
            <h4>Extra Features</h4>
            <hr>
        </div>
        <div data-field-type="table" data-field-name="extra_features" class="form-group col-12" element="div">
            <div class="array-container form-group">

                <table class="table table table-striped m-b-0 border">
                    <thead>
                        <tr>
                            <th style="font-weight: 600!important;">
                                Feature
                            </th>
                            <th style="font-weight: 600!important;">
                                Value
                            </th>

                            <th class="text-center"> </th>
                        </tr>
                    </thead>

                    <tbody class="table-striped items sortableOptions ui-sortable">
                        @foreach ($extras as $index => $item)
                            <tr class="array-row" style="">
                                <td>
                                    <input class="form-control form-control" type="text" data-cell-name="item.name"
                                        wire:model.defer="extras.{{ $index }}.name">
                                </td>
                                <td>
                                    <input class="form-control form-control" type="text" data-cell-name="item.desc"
                                        wire:model.defer="extras.{{ $index }}.desc">
                                </td>

                                <td>
                                    <button class="btn btn btn-light removeItem" type="button" style="width:100%"
                                        wire:click="removeExtra({{ $index }})">
                                        <i class="la la-trash" role="presentation" aria-hidden="true"></i>
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>

                </table>
                <div class="array-controls btn-group m-t-10">
                    <button class="btn btn btn-light" type="button" wire:click="addExtra()">
                        <i class="la la-plus"></i> Add extra feature
                    </button>
                </div>
            </div>
        </div>
    </div>
    <script>
        CKEDITOR.replace('content', {

            filebrowserBrowseUrl: '{{ asset('packages/ckfinder/ckfinder.html') }}',
            filebrowserImageBrowseUrl: '{{ asset('packages/ckfinder/ckfinder.html?type=Images') }}',
            filebrowserFlashBrowseUrl: '{{ asset('packages/ckfinder/ckfinder.html?type=Flash') }}',
            filebrowserUploadUrl: '{{ asset('packages/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
            filebrowserImageUploadUrl: '{{ asset('packages/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
            filebrowserFlashUploadUrl: '{{ asset('packages/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}',
            heading: {
                options: [{
                        model: 'paragraph',
                        title: 'Paragraph',
                        class: 'ck-heading_paragraph'
                    },
                    {
                        model: 'heading1',
                        view: 'h1',
                        title: 'Heading 1',
                        class: 'ck-heading_heading1'
                    },
                    {
                        model: 'heading2',
                        view: 'h2',
                        title: 'Heading 2',
                        class: 'ck-heading_heading2'
                    },
                    {
                        model: 'heading3',
                        view: 'h3',
                        title: 'Heading 3',
                        class: 'ck-heading_heading3'
                    },
                    {
                        model: 'heading4',
                        view: 'h4',
                        title: 'Heading 4',
                        class: 'ck-heading_heading4'
                    },
                    {
                        model: 'heading5',
                        view: 'h5',
                        title: 'Heading 5',
                        class: 'ck-heading_heading5'
                    },
                    {
                        model: 'heading6',
                        view: 'h6',
                        title: 'Heading 6',
                        class: 'ck-heading_heading6'
                    }
                ]
            }
        })
        document.querySelector('#submit').addEventListener(('click'), () => {
            @this.details = CKEDITOR.instances.content.getData();
            @this.saveAndBack();
        })

    </script>
</div>
