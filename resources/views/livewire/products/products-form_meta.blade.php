<div role="tabpanel" class="tab-pane" id="tab_metas">
    <div class="row">

        <!-- text input -->
        <div class="form-group col-sm-12" element="div"> <label>Meta Title</label>
            <input type="text" name="meta_title" value="" class="form-control">
        </div>

        <!-- text input -->
        <div class="form-group col-sm-12" element="div"> <label>Meta Description</label>
            <input type="text" name="meta_description" value="" class="form-control">
        </div>

        <!-- text input -->
        <div class="form-group col-sm-12" element="div"> <label>Meta Keywords</label>
            <input type="text" name="meta_keywords" value="" class="form-control">
        </div>
    </div>
</div>
