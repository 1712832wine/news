@php
$title = 'pages';
$title2 = 'page';
@endphp
<main class="main pt-2">
    @include('livewire.component.header-form',[$title, $type])

    <div class="container-fluid animated fadeIn">
        <div class="row">
            <div class="col-md-8 bold-labels">
                <!-- Default box -->
                <form>
                    <div class="card">
                        <div class="card-body row">
                            <div class="form-group col-md-6 required" element="div">
                                <label for="select_template">Template</label>
                                <select class="form-control" id="select_template" wire:model.defer="template">
                                    <option value="services">Services</option>
                                    <option value="about_us">About Us</option>
                                </select>
                                @error('template')
                                    <span class="error text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <!-- text input -->
                            <div class="form-group col-md-6 required" element="div">
                                <label for="page_name">Page name (only seen by admins)</label>
                                <input type="text" wire:model.defer="page_name" class="form-control" id="page_name">
                                @error('page_name')
                                    <span class="error text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <!-- text input -->
                            <div class="form-group col-sm-12 required" element="div">
                                <label for="page_title">Page Title</label>
                                <input type="text" wire:model.defer="page_title" id="page_title" class="form-control">
                                @error('page_title')
                                    <span class="error text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <!-- text input -->
                            <div class="form-group col-sm-12" element="div">
                                <label for="page_slug">Page Slug (URL)</label>
                                <input type="text" id="page_slug" wire:model.defer="page_slug" class="form-control">
                                <p class="help-block">Will be automatically generated from your title, if left empty.
                                </p>
                            </div>

                            <!-- used for heading, separators, etc -->
                            <div class="form-group col-sm-12" element="div"> <br>
                                <h2>Metas</h2>
                                <hr>
                            </div>

                            <!-- text input -->
                            <div class="form-group col-sm-12" element="div">
                                <label for="meta_title">Meta Title</label>
                                <input type="text" id="meta_title" wire:model.defer="meta_title" class="form-control">
                            </div>

                            <!-- text input -->
                            <div class="form-group col-sm-12" element="div">
                                <label for="meta_description">Meta Description</label>
                                <input type="text" id="meta_description" wire:model.defer="meta_description"
                                    class="form-control">
                            </div>

                            <!-- textarea -->
                            <div class="form-group col-sm-12" element="div">
                                <label for="meta_keyword">Meta Keywords</label>
                                <textarea id="meta_keyword" class="form-control"
                                    wire:model.defer="meta_keyword"></textarea>
                            </div>

                            <!-- used for heading, separators, etc -->
                            <div class="form-group col-sm-12" element="div"> <br>
                                <h2>
                                    <label for="content">
                                        Content
                                    </label>

                                </h2>
                                <hr>
                            </div> <!-- load the view from type and view_namespace attribute if set -->

                            <!-- CKeditor -->

                            <div class="form-group col-sm-12" element="div">
                                <textarea class="form-control" rows="4" id="content"
                                    wire:model.defer="meta_keyword"></textarea>
                            </div>
                        </div>
                    </div>

                    @include('livewire.component.saveaction-form')
                </form>
            </div>
        </div>



    </div>

</main>
