@if ($isOpen)
    @include('livewire.pages.pages-form')
@else
    @php
        $breadcrumb = [
            'items' => [
                [
                    'href' => route('dashboard'),
                    'title' => 'dashboard',
                ],
                [
                    'href' => route('pages'),
                    'title' => 'pages',
                ],
            ],
            'active' => 'List',
        ];
        $header = [
            'title' => 'Pages',
            'reset' => route('pages'),
        ];
        $button = 'Add page';
        $table_header = ['Name', 'Template', 'Slug', 'Actions'];
        $table_body = ['page_name', 'template', 'page_slug'];

    @endphp
    @include('livewire.component.main-component',[$breadcrumb, $header, $button, $table_header, $table_body,$table_actions=['clone'],$selectBox=false])
@endif
