@php
$title = 'Contacts';
$title2 = 'contact';
@endphp
<main class="main pt-2">
    @include('livewire.component.header-form',[$title, $type])
    <div class="container-fluid animated fadeIn">
        <div class="row">
            <div class="col-md-8 bold-labels">
                <form>
                    <div class="card">
                        <div class="card-body row">
                            {{-- Input text --}}
                            <div class="form-group col-sm-12">
                                <label for="name">Name</label>
                                <input type="text" id="name" class="form-control" wire:model.defer="name">
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="email">Email</label>
                                <input type="text" id="email" class="form-control" wire:model.defer="email">
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="address">Address</label>
                                <input type="text" id="address" class="form-control" wire:model.defer="address">
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="content">Content</label>
                                <input type="text" id="content" class="form-control" wire:model.defer="content">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div id="saveActions" class="form-group">
            <a href="" wire:click.prevent="closeModal()" class="btn btn-default">
                <span class="la la-ban"></span>
                &nbsp;Cancel
            </a>
        </div>
    </div>
</main>
