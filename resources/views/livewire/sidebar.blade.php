<div class="sidebar sidebar-pills bg-light">
    <nav class="sidebar-nav overflow-hidden ps">
        <ul class="nav">
            <li class="nav-title">Administration</li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('dashboard') }}">
                    <i class="nav-icon la la-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>

            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#">
                    <i class="nav-icon la la-newspaper-o"></i> News
                </a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('articles') }}">
                            <i class="nav-icon la la-newspaper-o"></i> <span>Articles</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('categories') }}">
                            <i class="nav-icon la la-list"></i> <span>Categories</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('tags') }}">
                            <i class="nav-icon la la-tag"></i> <span>Tags</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="{{ route('pages') }}">
                    <i class="nav-icon la la-file-o"></i>
                    <span>Pages</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('menu') }}">
                    <i class="nav-icon la la-list"></i>
                    <span>Menu</span>
                </a>
            </li>

            <!--Start Authentication-->
            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#">
                    <i class="nav-icon la la-group"></i> Authentication
                </a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('users') }}">
                            <i class="nav-icon la la-user"></i><span>Users</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('roles') }}">
                            <i class="nav-icon la la-group"></i><span>Roles</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('permissions') }}">
                            <i class="nav-icon la la-key"></i><span>Permissions</span>
                        </a>
                    </li>
                </ul>
            </li>
            <!--End Authentication-->

            <!--Start Advanced-->
            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#">
                    <i class="nav-icon la la-group"></i> Advanced
                </a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('file_manager') }}">
                            <i class="nav-icon la la-user"></i><span>File manager</span>
                        </a>
                    </li>

                </ul>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="{{ route('albums') }}">
                    <i class="nav-icon la la-file-image-o"></i>
                    <span>Albums</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('contacts') }}">
                    <i class="nav-icon la la-phone"></i>
                    <span>Contacts</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('recruitments') }}">
                    <i class="nav-icon la la-file-pdf-o"></i>
                    <span>Recruitment</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('products') }}">
                    <i class="nav-icon la la-shopping-cart"></i>
                    <span>Product</span>
                </a>
            </li>


        </ul>

    </nav>
</div>
