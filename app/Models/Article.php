<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use HasFactory;
    protected $fillable = [
        'id', 'title', 'slug', 'date', 'content','blocks_content','image','category','template','tags','status','featured'
    ];
}
