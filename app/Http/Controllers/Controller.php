<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Client\Request;

use App\Models\Article;
use App\Models\Album;
use App\Models\Asset;
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function getLoginStatus(Request $request)
    {
        $loginInfo = ['isLogin' => false];
        if ($user = auth()->user()) {
            $loginInfo = [
                'isLogin' => true,
                'userId' => $user->id,
                'email' => 'test@example.com',
                'name' => 'User1'
            ];
        }

        return response()->json($loginInfo);
    }
    // -------------------------------------------------------
    public function project() {
        $albums = Album::where([['type', 'project'],['status','PUBLISHED']])->orderBy('priority')->get();
        $lists = [];
        foreach($albums as $item) {
            $assets_of_item =  Asset::where('albums_id',$item['id'])->get();
            array_push($lists, $assets_of_item);
        }
        return view('frontend.project',['albums' => $albums,'lists'=> $lists])->layout('layouts.guest');
    }
    // -------------------------------------------------------
    // -------------------------------------------------------
    public function home(){
        $carousel_album = Album::where([['type', 'carousel home'],['status','PUBLISHED']])->first();
        $carousel_items=null;
        if($carousel_album!=null)
        $carousel_items =  Asset::where('albums_id',$carousel_album['id'])->get();
        $articles = Article::where([['category', 'home'],['status','PUBLISHED']])->get();
        $introduction = Article::where([['category', 'home introduction'],['status','PUBLISHED']])->first();
        $recruitment = Article::where([['category', 'home recruitment'],['status','PUBLISHED']])->first();
        return view('frontend.index',['articles' => $articles,'carousel_items' => $carousel_items,
        'introduction'=>$introduction,'recruitment'=>$recruitment]);
    }
    // -------------------------------------------------------
    public function introduce(){
        $albums = Album::where([['type', 'introduce'],['status','PUBLISHED']])->orderBy('priority','asc')->get();
        $lists = [];
        foreach($albums as $item) {
            $assets_of_item =  Asset::where('albums_id',$item['id'])->get();
            array_push($lists, $assets_of_item);
        }

        $articles =Article::where([['category', 'introduce'],['status','PUBLISHED']])->get();
        return view('frontend.introduce',['albums' => $albums,'lists'=> $lists,'articles' => $articles]);
    }
    // -------------------------------------------------------
    // -------------------------------------------------------
    public function newsdetail($slug){

        $article = Article::where('slug',$slug)->firstOrFail();
        switch ($article['template']) {
            case 'template 1':
                return view('frontend.news-001',['article' => $article]);
                break;
            default:
                return view('frontend.news-002',['article' => $article]);
                break;
        }
    }
}
