<?php

namespace App\Http\Livewire\Frontend;
use App\Models\Contact;
use Livewire\Component;

class AddContact extends Component
{
    public $name = '',$email = '',$address = '',$content= '', $item_id;
    public function resetInput(){
        $this->item_id = null;
        $this->name = '';
        $this->email = '';
        $this->address = '';
        $this->content = '';

    }
    public function saveAndBack()
    {
        Contact::updateOrCreate(['id' => $this->item_id], [
            'name' => $this->name,
            'email' => $this->email,
            'address' => $this->address,
            'content' => $this->content,
        ]);
        $this->resetInput();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Send Success!',
            'text'    => "We have received your feedback.",
        ]);
    }
    public function render()
    {
        return view('frontend.contact')->layout('layouts.guest');
    }
}
