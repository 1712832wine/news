<?php

namespace App\Http\Livewire\Frontend;
use App\Models\Article;
use Livewire\Component;
use Livewire\WithPagination;
class News extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $search = '';
    public $current = '';
    public function Do(){
        $this->search = $this->current;
        if($this->current=="")
        redirect('/tin-tuc');
    }
    public function render()
    {
        return view('frontend.news',[
            'interracts_articles' => Article::where([['category', 'news'],['status','PUBLISHED'],['tags','highest Interactive']])->get(),
            'articles'  => Article::where([['category', 'news'],['status','PUBLISHED']])->get(),
            'search_articles' => (Article::where([['category', 'news'],['status','PUBLISHED']])->where('title', 'like', '%'.$this->search.'%')->orwhere('content', 'like', '%'.$this->search.'%')->where([['category', 'news'],['status','PUBLISHED']]))->paginate(6),
        ])->layout('layouts.guest');
    }



}
