<?php

namespace App\Http\Livewire\Frontend;
use App\Models\Article;
use App\Models\Contact;
use Livewire\WithFileUploads;
use Livewire\Component;

class Recruitment extends Component
{
    use WithFileUploads;
    public $name='',$phone = '',$file='', $iteration=0;
    public function resetInput(){
        $this->name='';
        $this->phone='';
        $this->file='';
        $this->iteration++;
    }
    public function submit(){
        $name = '';
        if ($this->file){
        $name = md5($this->file . microtime()).'.'.$this->file->extension();
        $this->file->storeAs('photos', $name);
        }
        $type="recruitment";
        Contact::create([
            'name' => $this->name,
            'phone' => $this->phone,
            'email' => '',
            'address' => '',
            'content' => '',
            'type' => $type,
            'file' =>$name,
        ]);
        $this->resetInput();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Send Success!',
            'text'    => "We have received your application, please wait for the contact from HR",
        ]);
    }
    public function render()
    {
        $recruitment = Article::where([['category', 'recruitment'],['status','PUBLISHED']])->get();
        $short_desc = Article::where([['category', 'recruitment short_desc'],['status','PUBLISHED']])->first();
        return view('frontend.recruitment',['recruitment' => $recruitment,'short_desc' => $short_desc])->layout('layouts.guest');
    }
}
