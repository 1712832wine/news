<?php
namespace App\Http\Livewire\Albums;
use Livewire\Component;
use App\Models\Album;
use App\Models\Asset;
use App\Http\Livewire\Component\Alert;
use Illuminate\Support\Facades\Storage;

use Livewire\WithPagination;
use Livewire\WithFileUploads;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Str;
class AlbumsComponent extends Component
{
    use AuthorizesRequests;
    use WithFileUploads;
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    // css
    public $cssScope;
    // search
    public $search = '';
    public $pagination_size = 6;

    // album
    public $name,$short_desc,$content,$status,$title, $template, $item_id, $images = [],$dtype, $Asset,$priority;
    // asset
    public $a_item_id;
    public $type;
    public $isOpen = 0;
    protected $listeners = [
        'Album:delete' => 'delete',
        'Asset:delete' => 'deleteAsset',
        'ChangePriority:change' => 'changePriority',
        'Album:deleteSelected' => 'deleteSelected'
    ];
    public $selectAll =false, $selected=[];
    public function updatedSelectAll($value){
        if($value){
            $this->selected =
            Album::where('type', 'like', '%'.$this->search.'%')
            ->orWhere('name', 'like', '%'.$this->search.'%')->pluck('id');
        }else{
            $this->selected = [];
        }
    }
    public function mounted()
    {
        // initialize a scope at mount
        $this->cssScope = Str::random();
    }
    // pagination
    public function updatingPaginationSize()
    {
        $this->resetPage();
    }
    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        return view('livewire.albums.albums-component',[
            'list' => Album::where('type', 'like', '%'.$this->search.'%')
            ->orWhere('name', 'like', '%'.$this->search.'%')->orderBy('priority')->paginate($this->pagination_size),
            'photos' => Asset::all(),
            ]);
    }
    //  CREATE
    public function create($type)
    {
        $this->authorize('create', Album::class);
        $this->type = $type;
        $this->resetInputFields();
        $this->openModal();
    }
    // OPEN & CLOSE
    public function openModal()
    {
        $this->isOpen = 1;

    }
    public function closeModal()
    {
        $this->isOpen = 0;
    }
    public function openModalAsset(){


    }
    public function closeModalAsset()
    {
        $this->isOpen = 1;
    }
    // RESET INPUT
    private function resetInputFields(){
        $this->item_id=null;
        $this->name = '';
        $this->title = '';
        $this->priority = 'null';
        $this->short_desc = '';
        $this->template = '';
        $this->content = '';
        $this->dtype = '';
        $this->status = 'PUBLISHED';
        $this->images = [];
    }
    // SAVE AND BACK
    public function saveAndBack()
    {
        // validate
        $this->validate([
            'name'=>['required'],
        ]);
        // update
        Album::updateOrCreate(['id' => $this->item_id], [
            'name' => $this->name,
            'title' => $this->title,
            'template' => $this->template,
            'short_desc' =>$this->short_desc,
            'content' =>$this->content,
            'priority'=>$this->priority,
            'status' =>$this->status,
            'type' =>$this->dtype,
        ]);
        // find album id
        if ($this->type ==='edit') $album_id =  Album::find($this->item_id);
        else $album_id = Album::latest()->first();

        // update images
        foreach ($this->images as $image) {
            $name = md5($image . microtime()).'.'.$image->extension();
            $image->storeAs('photos', $name);
            Asset::updateOrCreate(['id' => $this->a_item_id], [
                'albums_id'=> $album_id->id,
                'url' =>   $name,
            ]);
        }
        $this->closeModal();
        $this->resetInputFields();
    }

    // EDIT
    public function edit($id, $type)
    {
        $this->authorize('update',Album::find($id));
        $this->type = $type;
        $Album = Album::findOrFail($id);
        $this->item_id = $id;
        $this->name = $Album->name;
        $this->title = $Album->title;
        $this->short_desc = $Album->short_desc;
        $this->template = $Album->template;
        $this->priority = $Album->priority;
        $this->content = $Album->content;
        $this->dtype = $Album->type;
        $this->status = $Album->status;
        $this->openModal();

    }
    public function editAsset($id, $type)
    {
        $this->authorize('update',Album::find($this->item_id));
        $this->type = $type;
        $this->Asset = Asset::findOrFail($id);
        redirect('admin/assets/'.$id,['id'=>$id]);

    }





    public function deleteSelected(){
        // return dd($this->selected)
        foreach ($this->selected as $album_id){
            $images = Asset::where('albums_id',$album_id)->get();
            foreach($images as $img) {
                Storage::delete('photos/'.$img['url']);
            }
            Asset::where('albums_id', $album_id)->delete();
        }
        Album::whereIn('id',$this->selected)->delete();
        $this->selected=[];
        $this->selectAll=false;
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Delete success!',
            'text'    => "These items has been deleted successfully",
        ]);
    }
    public function confirmDeleteSelected(){
        if (count($this->selected) > 0)
        {
            $this->authorize('delete',Album::find($this->selected[0]));
            $this->emit("swal:confirm", [
                'icon'        => 'warning',
                'type'        => 'warning',
                'title'       => 'Warning',
                'text'        => 'Are you sure you want to delete these items?',
                'confirmText' => 'Delete',
                'method'      => 'Album:deleteSelected',

            ]);
        }
        else
        {
            $this->emit('swal:modal', [
                'type'    => 'warning',
                'icon'    => 'warning',
                'title'   => 'Nothing to delete!',
                'text'    => "You have not choose any item to delete",
            ]);
        }
    }

    // DELETE
    public function delete($id)
    {
        $images = Asset::where('albums_id',$id)->get();
        foreach($images as $img) {
            Storage::delete('photos/'.$img['url']);
        }
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Delete Success!',
            'text'    => "The item has been deleted successfully",
        ]);
        Album::find($id)->delete();
        Asset::where('albums_id', $id)->delete();
    }
    public function confirmDelete($id) {
        $this->authorize('delete',Album::find($id));
        $this->emit("swal:confirm", [
            'icon'        => 'warning',
            'type'        => 'warning',
            'title'       => 'Warning',
            'text'        => "Are you sure you want to delete this item?",
            'confirmText' => 'Delete',
            'method'      => 'Album:delete',
            'params'      => $id, // optional, send params to success confirmation
        ]);
    }
    // DELETE ASSET
    public function deleteAsset($id)
    {
        $img = Asset::find($id)['url'];
        Storage::delete('photos/'.$img);
        Asset::find($id)->delete();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Delete Success!',
            'text'    => "The item has been deleted successfully",
        ]);
    }
    public function confirmDeleteAsset($id) {
        $this->authorize('delete',Album::find(Asset::find($id)->albums_id));
        $this->emit("swal:confirm", [
            'icon'        => 'warning',
            'type'        => 'warning',
            'title'       => 'Warning',
            'text'        => "Are you sure you want to delete this item?",
            'confirmText' => 'Delete',
            'method'      => 'Asset:delete',
            'params'      => $id, // optional, send params to success confirmation
        ]);
    }
    // CHANGE PRIORITY
    public function changePriority($order){
        $sorted = $order;
        sort($sorted, SORT_NATURAL | SORT_FLAG_CASE);
        foreach ($order as $index => $id){
            $album = Album::find($id);
            $album->priority = $sorted[$index];
            $album->save();
        }
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Change Priority Success!',
            'text'    => "These items have been change priority successfully",
        ]);
    }
    public function confirmChangePriority($order){
        $this->emit("swal:confirm", [
            'icon'        => 'warning',
            'type'        => 'warning',
            'title'       => 'Warning',
            'text'        => "Are you sure you want to change priority of this list?",
            'confirmText' => 'Change Priority',
            'method'      => 'ChangePriority:change',
            'params'      => $order, // optional, send params to success confirmation
        ]);

    }
}
