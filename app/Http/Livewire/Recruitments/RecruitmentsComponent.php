<?php

namespace App\Http\Livewire\Recruitments;

use Livewire\Component;
use Illuminate\Support\Facades\Storage;

use App\Models\Contact;
use App\Http\Livewire\Component\Alert;
use Livewire\WithPagination;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class RecruitmentsComponent extends Component
{
    use WithPagination;
    use AuthorizesRequests;
    protected $paginationTheme = 'bootstrap';
    // search
    public $search = '';
    // form
    public $item_id;
    public $pagination_size = 6;
    public $type;
    public $name = '',$phone='';
    // , $type, $link;
    public $isOpen = false;
    protected $listeners = ['recruitment:delete' => 'delete',
    'Recruitment:deleteSelected'=>'deleteSelected'];

    public $selectAll =false,$selected=[];
    // pagination
    public function updatingPaginationSize()
    {
        $this->resetPage();
    }
    public function updatingSearch()
    {
        $this->resetPage();
    }
    public function updatedSelectAll($value){
        if($value){
            $this->selected = 
            Contact::where([['name', 'like', '%'.$this->search.'%'],['type','recruitment']])->pluck('id');
        }else{
            $this->selected = [];
        }
        // return dd($this->selected);
    }
    public function render()
    {
        return view('livewire.recruitments.recruitments-component',[ 'list' => Contact::where([['name', 'like', '%'.$this->search.'%'],['type','recruitment']])->paginate($this->pagination_size)]);
    }

    public function download($id){
        $name = Contact::find($id)['file'];
        if ($name!==''){
            return response()->download('storage/photos/'.$name);;
        } else{
            $this->emit('swal:modal', [
                'type'    => 'warning',
                'icon'    => 'warning',
                'title'   => "Don't have file!",
                'text'    => "Don't have any file here",
            ]);
        }
        
    }
    // DELETE SELECTED
    public function deleteSelected(){
        $files = Contact::whereIn('id',$this->selected)->pluck('file');
        foreach ($files as $name)
        if ($name!==''){
            Storage::delete('photos/'.$name);
        }
        Contact::whereIn('id',$this->selected)->delete();
        $this->selected=[];
        $this->selectAll=false;
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Delete success!',
            'text'    => "These items has been deleted successfully",
        ]);
    }
    public function confirmDeleteSelected(){
        if (count($this->selected) > 0)
        {
            $this->authorize('delete',Contact::find($this->selected[0]));
            $this->emit("swal:confirm", [
                'icon'        => 'warning',
                'type'        => 'warning',
                'title'       => 'Warning',
                'text'        => "Are you sure you want to delete these items?",
                'confirmText' => 'Delete',
                'method'      => 'Recruitment:deleteSelected',
            
            ]);
        }
        else
        {
            $this->emit('swal:modal', [
                'type'    => 'warning',
                'icon'    => 'warning',
                'title'   => 'Nothing to delete!',
                'text'    => "You have not choose any item to delete",
            ]);
        }
    }
    // DELETE
    public function delete($id)
    {
        $name = Contact::find($id)['file'];
        if ($name!==''){
            Storage::delete('photos/'.$name);
        }

        Contact::find($id)->delete();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Delete Success!',
            'text'    => "The item has been deleted successfully",
        ]);
    }
    public function confirmDelete($id) {
        $this->authorize('delete',Contact::find($id));
        $this->emit("swal:confirm", [
            'icon'        => 'warning',
            'type'        => 'warning',
            'title'       => 'Warning',
            'text'        => "Are you sure you want to delete this item?",
            'confirmText' => 'Delete',
            'method'      => 'recruitment:delete',
            'params'      => $id, // optional, send params to success confirmation
        ]);
    }
}
