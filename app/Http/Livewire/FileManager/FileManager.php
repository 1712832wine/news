<?php

namespace App\Http\Livewire\FileManager;

use Livewire\Component;

class FileManager extends Component
{

    public function render()
    {
        return view('livewire.file-manager.file-manager');
    }

}
