<?php

namespace App\Http\Livewire\Categories;

use Livewire\Component;

use App\Models\Category;
use App\Http\Livewire\Component\Alert;
use Livewire\WithPagination;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class CategoriesComponent extends Component
{
    use AuthorizesRequests;
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $pagination_size = 6;
    // search
    public $search = '';
    // form
    public $name, $item_id, $slug, $parent,$currentName;


    public $type;
    // $link;
    public $isOpen = false;
    public $isShow = false;
    protected $listeners = ['category:delete' => 'delete','Category:deleteSelected'=>'deleteSelected'];

    public $selectAll =false, $selected=[];
    public function updatedSelectAll($value){
        if($value){
            $this->selected = Category::where('name', 'like', '%'.$this->search.'%')->pluck('id');
        }else{
            $this->selected = [];
        }
    }
    //  CRUD

    public function create($type)
    {
        $this->authorize('create', Category::class);
        $this->type = $type;
        $this->resetInputFields();
        $this->openModal();
    }

    public function edit($id, $type)
    {
        $this->authorize('update',Category::find($id));
        $this->type = $type;
        $category = Category::findOrFail($id);
        $this->item_id = $id;
        $this->name = $category->name;
        $this->currentName = $category->name;
        $this->slug = $category->slug;
        $this->parent = $category->parent;
        $this->openModal();
    }

    private function resetInputFields(){
        $this->item_id = null;
        $this->name='';
        $this->slug='';
        $this->parent='';
    }

    public function saveAndBack()
    {
        if($this->currentName)
        if($this->name==$this->currentName)
        Category::where('name',$this->name)->delete();
        $this->validate([
            'name' => ['required','unique:categories'],
        ]);
        Category::updateOrCreate(['id' => $this->item_id], [
            'name'=> $this->name,
            'slug'=> $this->slug,
            'parent'=> $this->parent,
            ]);
        $this->closeModal();
        $this->resetInputFields();
    }

    public function delete($id)
    {
        Category::find($id)->delete();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Delete Success!',
            'text'    => "The item has been deleted successfully",
        ]);
    }

    public function confirmDelete($id) {
        $this->authorize('delete',Category::find($id));
        $this->emit("swal:confirm", [
            'icon'        => 'warning',
            'type'        => 'warning',
            'title'       => 'Warning',
            'text'        => "Are you sure you want to delete this item?",
            'confirmText' => 'Delete',
            'method'      => 'category:delete',
            'params'      => $id, // optional, send params to success confirmation
        ]);
    }

    public function openModal()
    {
        $this->isOpen = true;
    }

    public function closeModal()
    {
        $this->isOpen = false;
    }

    public function openPreview($id){
        $this->isShow = true;
        $this->category = Category::findOrFail($id);
    }
    public function closePreview(){
        $this->isShow = false;
    }
    // pagination

    public function updatingPaginationSize()
    {
        $this->resetPage();
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    //clone
    public function clone($id){
        $this->authorize('update',Category::find($id));
        $Category = Category::findOrFail($id);
        //
        $this->name = $Category->name;
        $this->slug = $Category->slug;
        $this->parent = $Category->parent;
        //
        Category::create([
            'name' => $this->name.'(copy)',
            'slug' => $this->slug,
            'parent'  => $this->parent,
        ]);
        //
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Clone Success!',
            'text'    => "The item has been cloned successfully",
        ]);
    }


    public function render()
    {
        return view('livewire.categories.categories-component',[ 'list' => Category::where('name', 'like', '%'.$this->search.'%')->paginate($this->pagination_size)]);
    }

     // DELETE SELECTED
     public function deleteSelected(){
        Category::whereIn('id',$this->selected)->delete();
        $this->selected=[];
        $this->selectAll=false;
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Delete success!',
            'text'    => "These items has been deleted successfully",
        ]);
    }
    public function confirmDeleteSelected(){
        if (count($this->selected) > 0)
        {
            $this->authorize('delete',Category::find($this->selected[0]));
            $this->emit("swal:confirm", [
                'icon'        => 'warning',
                'type'        => 'warning',
                'title'       => 'Warning',
                'text'        => 'Are you sure you want to delete these items?',
                'confirmText' => 'Delete',
                'method'      => 'Category:deleteSelected',

            ]);
        }
        else
        {
            $this->emit('swal:modal', [
                'type'    => 'warning',
                'icon'    => 'warning',
                'title'   => 'Nothing to delete!',
                'text'    => "You have not choose any item to delete",
            ]);
        }
    }
}
