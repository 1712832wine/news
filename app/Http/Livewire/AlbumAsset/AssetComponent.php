<?php

namespace App\Http\Livewire\AlbumAsset;

use Livewire\Component;
use App\Models\Album;
use App\Models\Asset;
use Illuminate\Http\Request;

class AssetComponent extends Component
{
    public $type,$pid;

    // asset
    public $a_item_id ,$a_name ,$a_short_desc, $a_content ,$a_type,$a_url ,$a_priority ,$a_status,$albums_id;


    public function mount(Request $request)
    {
            $this->pid = $request->id;
            $this->Asset = Asset::find($request->id);
            $this->a_item_id = $this->Asset->id;
            $this->a_name = $this->Asset->name;
            $this->a_short_desc = $this->Asset->short_desc;
            $this->a_content = $this->Asset->content;
            $this->a_type = $this->Asset->type;
            $this->a_url = $this->Asset->url;
            $this->a_priority = $this->Asset->priority;
            $this->a_status = $this->Asset->status;
            $this->albums_id = $this->Asset->albums_id;
    }


    private function resetInputFieldsAsset(){
        $this->a_item_id = null;
        $this->a_name = '';
        $this->a_short_desc = '';
        $this->a_content = '';
        $this->a_type = '';
        $this->a_url = '';
        $this->a_priority = '';
        $this->a_status = '';
    }

    public function saveAndBack()
    {
        // validate
        // update album
        Asset::updateOrCreate(['id' => $this->pid], [
        'name' => $this->a_name,
        'short_desc' => $this->a_short_desc,
        'content' => $this->a_content,
        'type' => $this->a_type,
        'url' => $this->a_url,
        'priority' => $this->a_priority,
        'status' => $this->a_status,
        'albums_id' =>$this->albums_id
        ]);
        redirect('/admin/albums');
    }

    public function closeModal()
    {
        redirect('admin/albums');
    }





}
