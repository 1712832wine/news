<?php

namespace App\Http\Livewire\Contacts;

use Livewire\Component;


use App\Models\Contact;
use App\Http\Livewire\Component\Alert;
use Livewire\WithPagination;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class ContactsComponent extends Component
{
    use AuthorizesRequests;
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    // search
    public $search = '';
    // form
    public $item_id;
    public $pagination_size = 6;
    public $type;
    public $name = '',$email='',$address= '',$content='';
    // , $type, $link;
    public $isOpen = false;
    protected $listeners = ['Contact:delete' => 'delete','Contact:deleteSelected'=>'deleteSelected'];
    public $selectAll =false, $selected=[];
    // pagination
    public function updatedSelectAll($value){
        if($value){
            $this->selected = 
            Contact::where('type','!=','recruitment')
            ->orWhereNull('type')
            ->where('name', 'like', '%'.$this->search.'%')->pluck('id');
        }else{
            $this->selected = [];
        }
        // return dd($this->selected);
    }
    public function updatingPaginationSize()
    {
        $this->resetPage();
    }
    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        return view('livewire.contacts.contact-component',
        [ 'list' => Contact::where('type','!=','recruitment')
        ->orWhereNull('type')
        ->where('name', 'like', '%'.$this->search.'%')
        ->paginate($this->pagination_size)]);
    }
    //  CRUD
    public function openModal($id)
    {
        $Contact = Contact::find($id);
        $this->name=$Contact->name;
        $this->email=$Contact->email;
        $this->address=$Contact->address;
        $this->content=$Contact->content;
        $this->isOpen = true;
    }

    public function closeModal()
    {
        $this->isOpen = false;
    }

    public function deleteSelected(){
        Contact::whereIn('id',$this->selected)->delete();
        $this->selected=[];
        $this->selectAll=false;
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Delete success!',
            'text'    => "These items has been deleted successfully",
        ]);
    }
    public function confirmDeleteSelected(){
        if (count($this->selected) > 0)
        {
            $this->authorize('delete',Contact::find($this->selected[0]));
            $this->emit("swal:confirm", [
                'icon'        => 'warning',
                'type'        => 'warning',
                'title'       => 'Warning',
                'text'        => 'Are you sure you want to delete these items?',
                'confirmText' => 'Delete',
                'method'      => 'Contact:deleteSelected',
            
            ]);
        }
        else
        {
            $this->emit('swal:modal', [
                'type'    => 'warning',
                'icon'    => 'warning',
                'title'   => 'Nothing to delete!',
                'text'    => "You have not choose any item to delete",
            ]);
        }
    }


    public function delete($id)
    {
        Contact::find($id)->delete();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Delete Success!',
            'text'    => "The item has been deleted successfully",
        ]);

    }
    public function confirmDelete($id) {
        $this->authorize('delete',Contact::find($id));
        $this->emit("swal:confirm", [
            'icon'        => 'warning',
            'type'        => 'warning',
            'title'       => 'Warning',
            'text'        => "Are you sure you want to delete this item?",
            'confirmText' => 'Delete',
            'method'      => 'Contact:delete',
            'params'      => $id, // optional, send params to success confirmation
        ]);
    }
}
