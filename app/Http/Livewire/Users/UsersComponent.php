<?php


namespace App\Http\Livewire\Users;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;
use App\Models\Role;
use App\Models\Permission;
use App\Models\User;
use App\Http\Livewire\Component\Alert;
use Livewire\WithPagination;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
class UsersComponent extends Component
{
    use WithPagination;
    use AuthorizesRequests;
    protected $paginationTheme = 'bootstrap';
    // search
    public $search = '';
    // form
    public $Role_values, $Permission_values;
    public $name, $email, $password, $password_confirmation, $item_id;
    public $roles=[], $permissions=[];
    public $pagination_size = 6;
    public $type;
    public $isRole = [];
    // , $type, $link;
    public $isOpen = false;
    protected $listeners = ['User:delete' => 'delete','User:deleteSelected'=>'deleteSelected'];
    //select
    public $selectAll =false, $selected=[];
    public function updatedSelectAll($value){
        if($value){
            $this->selected = User::where('name', 'like', '%'.$this->search.'%')->pluck('id');
        }else{
            $this->selected = [];
        }
    }
    // pagination
    public function updatingPaginationSize()
    {
        $this->resetPage();
    }
    public function updatedRoles(){
        if (count($this->roles) === 1){
            $Role = Role::where('name', $this->roles)->first()['permissions'];
            $this->permissions = json_decode($Role);
            $this->isRole = $this->permissions;
        }
        elseif (count($this->roles) !== 0){
            foreach ($this->roles as $role)
            {
                // return dd($role);
                $extra = Role::where('name', $role)->first()['permissions'];
                // if ($role === 'SuperAdmin') return dd(json_decode($extra));
                $this->permissions = array_unique(array_merge($this->permissions,json_decode($extra)));
                // if ($role === 'SuperAdmin') return dd($this->permissions);
                $this->isRole = $this->permissions;
                // if ($role === 'SuperAdmin') return dd($this->isRole);
            }
        }else{
            $this->isRole =[];
        }
    }
    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        $this->Permission_values = Permission::all();
        $this->Role_values = Role::all();
        return view('livewire.users.users-component', ['list' => User::where('name', 'like', '%' . $this->search . '%')->paginate($this->pagination_size)]);
    }
    //  CRUD


    public function openModal()
    {
        $this->isOpen = true;
    }

    public function closeModal()
    {
        $this->isOpen = false;
    }

    private function resetInputFields()
    {
        $this->item_id='';
        $this->name = '';
        $this->email = '';
        $this->password = '';
        $this->password_confirmation = '';
        $this->roles = [];
        $this->permissions = [];

    }
    public function create($type)
    {
        $this->authorize('create', User::class);
        $this->type = $type;
        $this->resetInputFields();
        $this->openModal();
    }

    public function saveAndBack()
    {
        // validate
        if ($this->type ==='create') {
            $this->validate([
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'confirmed'],
            ]);
            // create
            User::create([
                'name' => $this->name,
                'email' => $this->email,
                'password' => Hash::make($this ->password),
                'role' =>json_encode(array_values($this->roles)),
                'permissions' =>json_encode(array_values($this->permissions))
            ]);
        } else {
            $this->validate([
                'name' => ['required', 'string', 'max:255'],
            ]);
            // update
            User::find($this->item_id)->update([
                'name' => $this->name,
                'role' =>json_encode(array_values($this->roles)),
                'permissions' =>json_encode(array_values($this->permissions))
            ]);
        }

        $this->closeModal();
        $this->resetInputFields();
    }

    public function edit($id, $type)
    {
        $this->authorize('update',User::find($id));
        $this->type = $type;
        $User = User::findOrFail($id);
        $this->item_id = $id;
        $this->name = $User->name;
        $this->password = '';
        $this->email = '';
        $this->roles = json_decode($User->role);
        $this->permissions = json_decode($User->permissions);
        $this->isRole =  $this->permissions;
        $this->openModal();
    }

    public function delete($id)
    {
        User::find($id)->delete();
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Delete Success!',
            'text'    => "The item has been deleted successfully",
        ]);
    }
    public function confirmDelete($id)
    {
        $this->authorize('delete',User::find($id));
        $this->emit("swal:confirm", [
            'icon'        => 'warning',
            'type'        => 'warning',
            'title'       => 'Warning',
            'text'        => "Are you sure you want to delete this item?",
            'confirmText' => 'Delete',
            'method'      => 'User:delete',
            'params'      => $id,
        ]);
    }

    //clone
    public function clone($id){
        $this->authorize('update',User::find($id));
        $User = User::findOrFail($id);
        //
        $this->name = $User->name;
        $this->email = $User->email;
        $this->password = $User->password;
        $this->password_confirmation = $User->password_confirmation;
        $this->roles = $User->roles;
        $this->permissions = $User->permissions;
        //
        User::create([
            'name' => $this->name.'(copy)',
            'email' => $this->email.'(copy)',
            'password' => $this->password,
            'password_confirmation' => $this->password_confirmation,
            'roles' => $this->roles,
            'permissions'  => $this->permissions,
        ]);
        //
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Clone Success!',
            'text'    => "The item has been cloned successfully",
        ]);
    }
     // DELETE SELECTED
     public function deleteSelected(){
        User::whereIn('id',$this->selected)->delete();
        $this->selected=[];
        $this->selectAll=false;
        $this->emit('swal:modal', [
            'type'    => 'success',
            'icon'    => 'success',
            'title'   => 'Delete success!',
            'text'    => "These items has been deleted successfully",
        ]);
    }
    public function confirmDeleteSelected(){
        if (count($this->selected) > 0)
        {
            $this->authorize('delete',User::find($this->selected[0]));
            $this->emit("swal:confirm", [
                'icon'        => 'warning',
                'type'        => 'warning',
                'title'       => 'Warning',
                'text'        => 'Are you sure you want to delete these items?',
                'confirmText' => 'Delete',
                'method'      => 'User:deleteSelected',

            ]);
        }
        else
        {
            $this->emit('swal:modal', [
                'type'    => 'warning',
                'icon'    => 'warning',
                'title'   => 'Nothing to delete!',
                'text'    => "You have not choose any item to delete",
            ]);
        }
    }
}
